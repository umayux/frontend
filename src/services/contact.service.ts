import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class ContactService {

    private base_api_url: string;
    public _current_keywords: Array<string>;

    constructor (private http: Http ) {
        this.base_api_url = environment.base_api_url;
    }

    send_email(name: string, email: string, message: string) {
        return this.http.post(this.base_api_url + 'contact/sendEmail/', {name: name, email: email, message: message})
        .map(res => {
            console.log(res);
            return res.json();
        });
    }
}
