import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class NLPService {

    private base_api_url: string;
    private polarity_base_url: string;
    private tags_base_url: string;
    public _current_keywords: Array<string>;

    constructor (private http: Http ) {
        this.base_api_url = environment.base_api_url;
        this.polarity_base_url = environment.polarity_api_url;
        this.tags_base_url = environment.tags_api_url;
    }

    polarity(sentence: string) {
        return this.http.post(this.base_api_url + '/predict/', {sentence: sentence})
        .map(res => {
            return res.json();
        });
    }

    tags(sentence: string) {
        return this.http.post(this.base_api_url + '/predict/', {sentence: sentence})
        .map(res => {
            return res.json();
        });
    }


}