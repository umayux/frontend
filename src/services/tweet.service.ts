import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../environments/environment';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class TweetService {
  private base_api_url: string;
  public _current_keywords: Array<string>;

  constructor(private http: Http) {
    this.base_api_url = environment.base_api_url;
  }

  sentiment_analysis(users: Array<Object>, analysis_type: string) {
    return this.http
      .post(this.base_api_url + '/sentiment/retrieve/most_relevant/', {
        users: users,
        analysis_type: analysis_type
      })
      .map(res => {
        return res.json();
      });
  }

  topic_analysis(users: Array<Object>, analysis_type: string) {
    return this.http
      .post(this.base_api_url + '/topics/retrieve/', {
        users: users,
        analysis_type: analysis_type
      })
      .map(res => {
        return res.json();
      });
  }

  current_keywords() {
    return this.http.get(this.base_api_url + 'retrieve/keywords').map(res => {
      this._current_keywords = res.json();
    });
  }

  tweet_history(keyword: string) {
    return this.http
      .get(this.base_api_url + 'retrieve/historical_data/' + keyword)
      .map(res => {
        return res.json();
      });
  }

  tweet_history_10h(keyword: string) {
    return this.http
      .get(this.base_api_url + 'retrieve/sentiment_10h/' + keyword)
      .map(res => {
        return res.json();
      });
  }

  tweet_history_total(keyword: string) {
    return this.http
      .get(this.base_api_url + 'retrieve/historical_total/' + keyword)
      .map(res => {
        return res.json();
      });
  }

  tweet_status(keyword: string) {
    return this.http
      .get(this.base_api_url + 'retrieve/profile_status/' + keyword)
      .map(res => {
        return res.json();
      });
  }

  most_voted_tweet(keyword: string) {
    return this.http
      .get(this.base_api_url + 'retrieve/most_voted_tweet/' + keyword)
      .map(res => {
        return res.json();
      });
  }
}
