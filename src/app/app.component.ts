import { Component } from '@angular/core';
import { TweetService } from '../services/tweet.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    public tweetService: TweetService
  ) {
    this.tweetService.current_keywords()
      .subscribe( res => {
        // console.log(this.tweetService._current_keywords);
      });
  }
  // retrieve the keyrowds that are being tracked



}
