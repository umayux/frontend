import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-main-panel',
  templateUrl: './main-panel.component.html',
  styleUrls: ['./main-panel.component.css']
})
export class MainPanelComponent implements OnInit, OnChanges {

  @Input()
  _keywords: any;

  ngOnInit() {}
  ngOnChanges(changes: SimpleChanges) {

    if (changes['_keywords']) {
      // console.log(this._keywords);
    }
  }
}
