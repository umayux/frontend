import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserVolumeComponent } from './user-volume.component';

describe('UserVolumeComponent', () => {
  let component: UserVolumeComponent;
  let fixture: ComponentFixture<UserVolumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserVolumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserVolumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
