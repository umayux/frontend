import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostHatedTweetComponent } from './most-hated-tweet.component';

describe('MostHatedTweetComponent', () => {
  let component: MostHatedTweetComponent;
  let fixture: ComponentFixture<MostHatedTweetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostHatedTweetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostHatedTweetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
