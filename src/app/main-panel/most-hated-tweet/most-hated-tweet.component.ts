import { Component, OnInit } from '@angular/core';
import { HighCharts } from '../../../classes/wrappers/HighCharts'


@Component({
  selector: 'app-most-hated-tweet',
  templateUrl: './most-hated-tweet.component.html',
  styleUrls: ['./most-hated-tweet.component.css']
})
export class MostHatedTweetComponent implements OnInit {
  public tweet: Object;
  public highcharts: any;
  public data: Object;

  constructor() { }

  ngOnInit() {
    this.tweet = {
      author: "German Vargas Lleras",
      text: "Vamos a implementar en el país el más ambicioso y amplio programa en materia de red terciaria. Atenderemos al menos 20 km de vías de la red terciaria por municipio. #MejorAgricultura"
    }


    this.data = [{
      name: 'Reacciones Positivas',
      colorByPoint: true,
      data: [{
          name: 'Positivo',
          y: 6.33,
      }, {
          name: 'Neutral',
          y: 4.03,
      }, {
          name: 'Negativo',
          y: 80.38
      }]
  }]

  // 
  this.highcharts = new HighCharts();
  this.highcharts.column("most-hated-pie-chart-01", this.data)


  }

}
