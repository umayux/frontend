import { Component, OnInit } from '@angular/core';
import { HighCharts } from '../../../classes/wrappers/HighCharts'


@Component({
  selector: 'app-most-loved-tweet',
  templateUrl: './most-loved-tweet.component.html',
  styleUrls: ['./most-loved-tweet.component.css']
})
export class MostLovedTweetComponent implements OnInit {

  public tweet: Object;
  public highcharts: any;
  public data: Object;
  constructor() { }

  ngOnInit() {
    this.tweet = {
      author: "Gustavo Petro",
      text: "La manifestación de Cartagena. El pueblo caribe con la Colombia Humana"
    }

    this.data = [{
        name: 'Reacciones Positivas',
        colorByPoint: true,
        data: [{
            name: 'Positivo',
            y: 56.33,
        }, {
            name: 'Neutral',
            y: 24.03,
        }, {
            name: 'Negativo',
            y: 10.38
        }]
    }]

    // 
    this.highcharts = new HighCharts();
    this.highcharts.column("most-loved-pie-chart-01", this.data)

  }

}
