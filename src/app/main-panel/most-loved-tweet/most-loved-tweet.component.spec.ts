import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MostLovedTweetComponent } from './most-loved-tweet.component';

describe('MostLovedTweetComponent', () => {
  let component: MostLovedTweetComponent;
  let fixture: ComponentFixture<MostLovedTweetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MostLovedTweetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MostLovedTweetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
