import { Component, OnInit } from '@angular/core';
// import { WordCloud } from '../../../classes/wrappers/WordCloud'
import { HighCharts } from '../../../classes/wrappers/HighCharts';


@Component({
  selector: 'app-topics',
  templateUrl: './topics.component.html',
  styleUrls: ['./topics.component.css']
})
export class TopicsComponent implements OnInit {
  public data: any;
  public wordcloud: any;

  constructor() {

  }

  ngOnInit() {
    this.data = [
      {name: 'politica', weight: 16},
      {name: 'castrochavismo', weight: 2},
      {name: 'corrupcion', weight: 12},
      {name: 'parapolitica', weight: 10},
      {name: 'farc', weight: 9},
      {name: 'presidente', weight: 8},
      {name: 'cambioradical', weight: 5},
      {name: 'progresistas', weight: 3},
      {name: 'venezuela', weight: 7},
      {name: 'uribe', weight: 7},
      {name: 'santos', weight: 7},
      {name: 'verdad', weight: 7},
    ];

    this.wordcloud = new HighCharts();
    this.wordcloud.wordcloud('topics-word-cloud-positive', this.data);
    this.wordcloud.wordcloud('topics-word-cloud-negative', this.data);

  }
}
