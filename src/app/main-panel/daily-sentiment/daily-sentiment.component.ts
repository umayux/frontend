import {
  Component,
  OnInit,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';
import { HighCharts } from '../../../classes/wrappers/HighCharts';

// services
import { TweetService } from '../../../services/tweet.service';
import { LDA } from '../../../classes/wrappers/LdaModel';

@Component({
  selector: 'app-daily-sentiment',
  templateUrl: './daily-sentiment.component.html',
  styleUrls: ['./daily-sentiment.component.css']
})
export class DailySentimentComponent implements OnInit, OnChanges, OnDestroy {
  public profiles: any;
  public _render: boolean;
  public Math: any;
  public current_time: any;
  public _interval_id: any;
  public rating: any;
  public lda: any;

  @Input() _keywords: any;

  constructor(private tweetService: TweetService) {}

  ngOnInit() {
    this.profiles = [];
    this._render = false;
    this.Math = Math;
    this.current_time = new Date().getTime();
    this._interval_id = null;
    this.rating = { starting: 5 };
    this.lda = new LDA();
  }

  ngOnChanges(changes: SimpleChanges) {
    // historical data
    if (changes['_keywords']) {
      if (this._keywords) {
        // execute historical data
        this.retrieve_historical_data(this._keywords);
        // this.profiles = [];
        // execute each 10 seconds make a request
        this._interval_id = setInterval(() => {
          this.current_time = new Date().getTime();
          this.retrieve_historical_data(this._keywords);
          // this.profiles = [];
        }, 30000);
      }
    }
  }

  ngOnDestroy() {
    if (this._interval_id) {
      clearInterval(this._interval_id);
    }
  }

  update_profile(profiles: any, res: any) {
    const i = res.status[0].data[0].user.name;
    profiles[i].status[0].data[0].user.profile_image_url =
      res.status[0].data[0].user.profile_image_url;
    profiles[i].sentiment[0].data[0].n_positive =
      res.sentiment[0].data[0].n_positive;
    profiles[i].sentiment[0].data[0].n_negative =
      res.sentiment[0].data[0].n_negative;
    profiles[i].sentiment[0].data[0].n_neutral =
      res.sentiment[0].data[0].n_neutral;
    profiles[i].status[0].data[0].user.name = res.status[0].data[0].user.name;
    profiles[i].status[0].data[0].user.screen_name =
      res.status[0].data[0].user.screen_name;
    profiles[i].status[0].data[0].created_at = res.status[0].data[0].created_at;
    profiles[i].status[0].data[0].timestamp_ms =
      res.status[0].data[0].timestamp_ms;
    try {
      profiles[i].status[0].data[0].extended_tweet.full_text =
        res.status[0].data[0].extended_tweet.full_text;
    } catch {
      profiles[i].status[0].data[0].text = res.status[0].data[0].text;
    }

    profiles[i].tags[0].data[0] = res.tags[0].data[0];
  }

  retrieve_historical_data(keywords: Array<string>) {
    keywords.forEach((e, i) => {
      // traverse the keyword list and get the data for each one of the keywords,
      // then add it to the historical-data variable.
      this.tweetService.tweet_status(e).subscribe(res => {
        // console.log(res);

        // const _sentiment = res.sentiment[0].data[0];
        // const _status = res.status[0].data[0];
        // const _tags = res.sentiment[0].data[0];

        if (this.profiles.length === keywords.length) {
          this.profiles[i] = res;
          // res.render = true;
          // this.update_profile(this.profiles, res);
        } else {
          // res.render = true;
          // setTimeout(() => {
          this.profiles.push(res);
          // this.profiles[_status.user.name] = res;
          // }, 100);
        }

        // check if the historical_data variable has been populated by all the keyword's data
        if (this.profiles.length === 1) {
          // console.log(this.profiles);

          this._render = true;

          // this.profiles[i].render = true;
          // console.log(this.profiles);
          // this.event_drops.render(this.historical_data, "_historical_sentiment_chart");
        }
      });
    });
  }
}
