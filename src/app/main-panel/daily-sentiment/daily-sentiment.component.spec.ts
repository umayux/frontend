import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailySentimentComponent } from './daily-sentiment.component';

describe('DailySentimentComponent', () => {
  let component: DailySentimentComponent;
  let fixture: ComponentFixture<DailySentimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailySentimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailySentimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
