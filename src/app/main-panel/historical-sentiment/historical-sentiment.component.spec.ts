import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricalSentimentComponent } from './historical-sentiment.component';

describe('HistoricalSentimentComponent', () => {
  let component: HistoricalSentimentComponent;
  let fixture: ComponentFixture<HistoricalSentimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricalSentimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricalSentimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
