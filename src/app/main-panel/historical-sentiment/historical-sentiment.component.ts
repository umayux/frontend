import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';

import { EventDrops } from '../../../classes/wrappers/EventDrops';

// services
import { TweetService } from '../../../services/tweet.service';
import { HighCharts } from '../../../classes/wrappers/HighCharts';

// import { LDA } from '../../../classes/wrappers/LdaModel';

@Component({
  selector: 'app-historical-sentiment',
  templateUrl: './historical-sentiment.component.html',
  styleUrls: ['./historical-sentiment.component.css']
})
export class HistoricalSentimentComponent implements OnInit, OnChanges {
  public event_drops: any = new EventDrops();
  public keywords: any;
  public historical_positive_data: Array<Object>;
  public historical_negative_data: Array<Object>;
  public repositoriesData: Array<Object>;
  private render: boolean;
  public highcharts: any;
  // public lda: any;

  constructor(
    private tweetService: TweetService,
    private ref: ChangeDetectorRef
  ) {}

  @Input() _keywords: any;

  ngOnInit() {
    this.historical_positive_data = [];
    this.historical_negative_data = [];
    this.render = false;
    this.highcharts = new HighCharts();
    // this.lda = new LDA();
    // console.log(
    //   this.lda.topics([
    //     'el dia de hoy todo fue bien',
    //     'feliz viernes de rumba',
    //     'gozar la vida en este dia'
    //   ])
    // );
    // LDA.topics()
  }

  ngOnChanges(changes: SimpleChanges) {
    // historical data
    if (changes['_keywords']) {
      if (this._keywords) {
        this.retrieve_historical_data(this._keywords);
      }
    }
  }

  retrieve_historical_data(keywords: Array<string>) {
    keywords.forEach((e, i) => {
      // traverse the keyword list and get the data for each one of the keywords,
      // then add it to the historical-data variable.
      this.tweetService.tweet_history_10h(e).subscribe(res => {
        // console.log(res);
        this.historical_positive_data.push({
          name: res.name,
          data: res.data.map(e => e.n_positive),
          categories: res.from.map(e => {
            const dt = new Date(e);
            return dt.toLocaleString('en-US', {
              hour: 'numeric',
              hour12: true
            });
          })
        });

        this.historical_negative_data.push({
          name: res.name,
          data: res.data.map(
            e =>
              100 *
              (e.n_positive / (e.n_positive + e.n_negative + e.n_neutral) -
                e.n_negative / (e.n_positive + e.n_negative + e.n_neutral))
          ),
          categories: res.from.map(e => {
            const dt = new Date(e);
            return dt.toLocaleString('en-US', {
              hour: 'numeric',
              hour12: true
            });
          })
        });

        // check if the historical_data variable has been populated by all the keyword's data
        if (this.historical_positive_data.length === keywords.length) {
          // this.event_drops.render(this.historical_data, '_historical_sentiment_chart');
          this.highcharts.area_spline(
            '_historical_positive_volume',
            this.historical_positive_data
          );

          // console.log(this.historical_positive_data);

          this.render = true;

          // console.log(this.historical_positive_data);
        }
      });
    });
  }

  render_favorability() {
    // console.log('datadatadatadata');
    this.highcharts.area_spline(
      '_historical_negative_volume',
      this.historical_negative_data
    );
  }
}
