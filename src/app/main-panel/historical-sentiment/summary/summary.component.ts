import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';
import { HighCharts } from '../../../../classes/wrappers/HighCharts';
// services
import { TweetService } from '../../../../services/tweet.service';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnChanges, OnInit {
  public highcharts: any;
  public data: Object;
  public historical_data: Array<Object>;
  public historical_data_sort: Array<Object>;
  private render: boolean;
  private countMessages: any;
  private individualCount: any;

  constructor(
    private tweetService: TweetService,
    private ref: ChangeDetectorRef
  ) {}

  @Input() _keywords: any;

  ngOnChanges(changes: SimpleChanges) {
    // historical data
    if (changes['_keywords']) {
      if (this._keywords) {
        this.retrieve_historical_data(this._keywords);
      }
    }
  }

  ngOnInit() {
    this.historical_data = [];
    this.historical_data_sort = new Array<object>();
    this.render = false;
  }

  retrieve_historical_data(keywords: Array<string>) {
    keywords.forEach((e, i) => {
      // traverse the keyword list and get the data for each one of the keywords,
      // then add it to the historical-data variable.
      this.tweetService.tweet_history_total(e).subscribe(res => {
        this.historical_data.push(res);
        // check if the historical_data variable has been populated by all the keyword's data
        if (this.historical_data.length === keywords.length) {
          this.set_values(this.historical_data);
          this.render = true;
        }
      });
    });
  }

  set_values(historicalData: any) {
    this.countMessages = 0;
    historicalData.forEach(element => {
      this.countMessages += element.value;
    });

    console.log(this.historical_data);

    const repositoriesData = historicalData.map(repository => ({
      name: repository.name,
      y: 100 * repository.value / this.countMessages
    }));

    this.data = [
      {
        name: 'encuesta',
        // colorByPoint: true,
        data: repositoriesData
      }
    ];

    this.highcharts = new HighCharts();
    this.highcharts.barplot('survey', this.data);
    // console.log(this.data)
  }
}
