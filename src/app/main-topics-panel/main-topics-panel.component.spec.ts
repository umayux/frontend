import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainTopicsPanelComponent } from './main-topics-panel.component';

describe('MainTopicsPanelComponent', () => {
  let component: MainTopicsPanelComponent;
  let fixture: ComponentFixture<MainTopicsPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainTopicsPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainTopicsPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
