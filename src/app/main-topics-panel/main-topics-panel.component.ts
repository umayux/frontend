import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-topics-panel',
  templateUrl: './main-topics-panel.component.html',
  styleUrls: ['./main-topics-panel.component.css']
})
export class MainTopicsPanelComponent implements OnInit {

  public date: any;

  constructor() { }

  ngOnInit() {
    this.date = new Date();
  }

}
