import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-right-panel',
  templateUrl: './right-panel.component.html',
  styleUrls: ['./right-panel.component.css']
})
export class RightPanelComponent implements OnInit {

  private render: boolean;

  constructor() { }

  ngOnInit() {
    this.render = false;
    setTimeout( () => {
      this.render = true;
    }, 1000);
  }

}
