import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MytagsComponent } from './mytags.component';

describe('MytagsComponent', () => {
  let component: MytagsComponent;
  let fixture: ComponentFixture<MytagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MytagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MytagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
