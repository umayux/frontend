import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MysentimentComponent } from './mysentiment.component';

describe('MysentimentComponent', () => {
  let component: MysentimentComponent;
  let fixture: ComponentFixture<MysentimentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MysentimentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MysentimentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
