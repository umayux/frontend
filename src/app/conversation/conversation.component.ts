import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';

import { TweetService } from '../../services/tweet.service';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit, OnChanges {
  constructor(
    private tweetService: TweetService,
  ) {}

  @Input() _keywords: any;

  ngOnInit() {}

  ngOnChanges(changes: SimpleChanges) {
    // historical data
    if (changes['_keywords']) {
      if (this._keywords) {
        // this.retrieve_historical_data(this._keywords);
      }
    }
  }
}
