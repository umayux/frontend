import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeftPanelComponent } from './left-panel/left-panel.component';
import { MainPanelComponent } from './main-panel/main-panel.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MainTopicsPanelComponent } from './main-topics-panel/main-topics-panel.component';
import { FooterComponent } from './footer/footer.component';
import { HistoricalSentimentComponent } from './main-panel/historical-sentiment/historical-sentiment.component';
import { TopicsComponent } from './main-panel/topics/topics.component';
import { UserVolumeComponent } from './main-panel/user-volume/user-volume.component';
import { MostHatedTweetComponent } from './main-panel/most-hated-tweet/most-hated-tweet.component';
import { MostLovedTweetComponent } from './main-panel/most-loved-tweet/most-loved-tweet.component';
import { DailySentimentComponent } from './main-panel/daily-sentiment/daily-sentiment.component';
import { ContactComponent } from './left-panel/contact/contact.component';

import { TweetService } from '../services/tweet.service';
import { ContactService } from '../services/contact.service';

import { StarRatingModule } from 'angular-star-rating';
import { Ng4TwitterTimelineModule } from 'ng4-twitter-timeline/lib/index';
import { RightPanelComponent } from './right-panel/right-panel.component';
import { MysentimentComponent } from './right-panel/mysentiment/mysentiment.component';
import { MytagsComponent } from './right-panel/mytags/mytags.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SummaryComponent } from './main-panel/historical-sentiment/summary/summary.component';
import { ConversationComponent } from './conversation/conversation.component';

import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatTableModule } from '@angular/material/table';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatGridListModule } from '@angular/material/grid-list';
import { QuickAccessComponent } from './quick-access/quick-access.component';

@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    MainPanelComponent,
    NavbarComponent,
    MainTopicsPanelComponent,
    FooterComponent,
    HistoricalSentimentComponent,
    TopicsComponent,
    UserVolumeComponent,
    MostHatedTweetComponent,
    MostLovedTweetComponent,
    DailySentimentComponent,
    RightPanelComponent,
    MysentimentComponent,
    MytagsComponent,
    ContactComponent,
    SummaryComponent,
    ConversationComponent,
    QuickAccessComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    HttpModule,
    StarRatingModule.forRoot(),
    Ng4TwitterTimelineModule,
    BrowserAnimationsModule,
    MatTabsModule,
    MatCardModule,
    MatIconModule,
    MatButtonModule,
    MatChipsModule,
    MatTableModule,
    MatSidenavModule,
    MatGridListModule
  ],
  providers: [TweetService, ContactService],
  bootstrap: [AppComponent],
  exports: [SummaryComponent]
})
export class AppModule {}
