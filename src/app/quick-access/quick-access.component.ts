import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges,
  ChangeDetectorRef
} from '@angular/core';

import { TweetService } from '../../services/tweet.service';
import { LDA } from '../../classes/wrappers/LdaModel';

@Component({
  selector: 'app-quick-access',
  templateUrl: './quick-access.component.html',
  styleUrls: ['./quick-access.component.css']
})
export class QuickAccessComponent implements OnInit, OnChanges {
  public lda: any;
  public data: any;

  constructor(private tweetService: TweetService) {}

  @Input() _keywords: any;

  ngOnInit() {
    this.lda = new LDA();
    this.data = { comments: [] };
  }

  ngOnChanges(changes: SimpleChanges) {
    // historical data
    if (changes['_keywords']) {
      if (this._keywords) {
        this.get_user_info(this._keywords[0]);
        // this.retrieve_historical_data(this._keywords);
      }
    }
  }

  get_user_info(keyword: string) {
    this.tweetService.most_voted_tweet(keyword).subscribe(res => {
      // console.log(res);
      // res['topics'] = this.lda.topics(res.comments.map(e => e._text));
      this.data = res;
      console.log(this.data);
    });
  }
}
