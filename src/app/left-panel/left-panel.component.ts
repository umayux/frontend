import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.css']
})
export class LeftPanelComponent implements OnInit {

  private render: boolean;
  private _date_now: any;
  constructor() { }

@Input()
_names: string;
_email: string;
_message: string;

  ngOnInit() {
    this.render = false;
    setTimeout( () => { this.render = true; }, 500);
    this._date_now = new Date();
  }

  spanishDate(d) {
    const weekday = ['Domingo', "Lunes", 'Martes', 'Miercoles', 'Jueves','Viernes','Sabado'];
    const monthname = ['Enero', 'Febrero', 'Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    return weekday[d.getDay()]+' '+d.getDate()+' de '+monthname[d.getMonth()] +' de '+d.getFullYear() + ' - ' + d.getHours() + ':' + d.getMinutes();
    }

}
