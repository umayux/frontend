import {
  Component,
  OnInit,
  ChangeDetectorRef,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
// services
import { ContactService } from '../../../services/contact.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
public success: string;
  constructor(private contactService: ContactService,
    private  ref: ChangeDetectorRef) {

  }


ngOnInit() {
this.success = '';

}

send_email_click(names: string, email: string, message: string) {
this.success = 'Enviando...';
this.contactService.send_email(names, email, message).subscribe(res => {
this.success = 'Su mensaje ha sido enviado';
setTimeout(() => {
  this.success = '';
}, 2000);

});
}



}
