declare var require: any;
const lda = require('lda/lib/lda.js');

export class LDA {
  constructor() {}

  topics(documents: any) {
    // const text =
    //   'Cats are small. Dogs are big. Cats like to chase mice. Dogs like to eat bones.';
    // const documents = text.match(/[^\.!\?]+[\.!\?]+/g);
    return lda(documents, 3, 3, ['es', 'en']);
  }
}
