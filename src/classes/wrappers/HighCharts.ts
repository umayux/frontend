declare var require: any;
var highcharts = require('highcharts/highcharts.js');
require('highcharts/modules/wordcloud.js')(highcharts);

export class HighCharts {
  constructor() {}

  compare(a, b) {
    if (a.y < b.y) {
      return 1;
    }
    if (a.y > b.y) {
      return -1;
    }
    return 0;
  }

  barplot(selector: string, data: any) {
    // console.log(data)
    data[0].data.sort(this.compare);
    highcharts.chart(selector, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'bar',
        width: null,
        height: data.length * 15 + 300,
        marginTop: 10,
        borderWidth: 0,
        borderColor: '#000'
      },
      title: {
        text: null,
        margin: 0
      },
      xAxis: {
        visible: true,
        offset: 2,
        color: '#000',
        linewidth: 1,
        labels: {
          enabled: true,
          style: {
            color: '#000'
          }
        },
        categories: data[0].data.map(e => e.name)
      },
      yAxis: {
        visible: false,
        title: {
          text: null
          // align: 'high'
        },
        labels: {
          enabled: true
        },
        min: 0,
        startOnTick: true,
        minorGridLineWidth: 1,
        majorGridLineWidth: 1,
        gridLineWidth: 1,
        // gridLineColor: '#000',
        tickWidth: 0
      },
      pane: {
        // center: ['50%', '50%']
      },
      tooltip: {
        enabled: false,
        pointFormat: '{point.y:.1f}%'
      },
      plotOptions: {
        series: {
          colorByPoint: true,
          groupPadding: 0,
          stacking: 'normal',
          borderColor: '#000',
          borderWidth: 0
        },
        bar: {
          borderRadius: 0,
          showInLegend: true,
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: 0,
            format: '({point.y:.1f} %)'
          }
        }
      },
      series: data,
      credits: {
        enabled: false
      },
      legend: {
        enabled: false
      }
    });
  }

  column(selector: string, data: any) {
    highcharts.chart(selector, {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'column',
        width: null,
        height: data.length * 15 + 300,
        marginTop: 10,
        borderWidth: null
      },
      title: {
        text: null,
        margin: 0
      },
      xAxis: {
        visible: false,
        offset: 2,
        color: null,
        linewidth: 0,
        labels: {
          enabled: false
        }
      },
      yAxis: {
        visible: false,
        title: {
          text: 'Comentarios'
        },
        labels: {
          enabled: false
        },
        min: -1,
        startOnTick: false,
        minorGridLineWidth: 0,
        majorGridLineWidth: 0,
        gridLineWidth: 0,
        tickWidth: 1
      },
      pane: {
        center: ['50%', '50%']
      },
      tooltip: {
        enabled: false,
        pointFormat: '{series.name}'
      },
      plotOptions: {
        series: {
          // borderColor: "#000",
        },
        column: {
          borderRadius: 5,
          showInLegend: false,
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: true,
            distance: 10,
            format: '<b>{point.name}</b> ({point.y:.1f} %)'
          }
        }
      },
      series: data,
      credits: {
        enabled: false
      }
    });
  }

  wordcloud(selector: string, data: any) {
    highcharts.chart(selector, {
      chart: {
        plotBorderWidth: 0
      },
      series: [
        {
          type: 'wordcloud',
          data: data,
          name: 'Usuarios'
        }
      ],
      title: {
        text: null
      },
      credits: {
        enabled: false
      }
    });
  }

  area(selector: string, data: any) {
    highcharts.chart(selector, {
      chart: {
        type: 'line',
        width: null,
        height: data.length * 15 + 300,
        marginTop: 10
        // marginLeft: 2,
        // marginRight: 2,
        // marginBottom: 40
      },
      title: {
        text: null
      },
      subtitle: {
        text: null
      },
      xAxis: {
        type: 'datetime',
        allowDecimals: false,
        labels: {
          formatter: function() {
            return this.value; // clean, unformatted number for year
          }
        }
      },
      yAxis: {
        title: {
          text: null
        },
        labels: {
          formatter: function() {
            return this.value / 1000 + 'k';
          }
        }
      },
      tooltip: {
        // pointFormat: null
      },
      plotOptions: {
        area: {
          pointStart: 1940,
          marker: {
            enabled: false,
            symbol: 'circle',
            radius: 2,
            states: {
              hover: {
                enabled: true
              }
            }
          }
        }
      },
      series: data,
      credits: {
        enabled: false
      }
    });
  }

  area_spline(selector: string, data: any) {
    highcharts.chart(selector, {
      chart: {
        type: 'areaspline',
        height: data.length * 15 + 191
      },
      title: {
        text: null
      },
      legend: {
        layout: 'horizontal',
        align: 'center',
        verticalAlign: 'top',
        // x: 150,
        // y: 100,
        floating: true,
        borderWidth: 0,
        backgroundColor: 'rgba(255,255,255,0)'
      },
      xAxis: {
        categories: data[0].categories
        // plotBands: [
        //   {
        //     // visualize the weekend
        //     from: 4.5,
        //     to: 5.5,
        //     color: 'rgba(68, 170, 213, .2)'
        //   }
        // ]
      },
      yAxis: {
        title: {
          text: null
        }
      },
      tooltip: {
        shared: true,
        valueSuffix: ''
        // pointFormat: '{point.y:.2f}'
      },
      credits: {
        enabled: false
      },
      plotOptions: {
        areaspline: {
          fillOpacity: 0.1
        }
      },
      series: data
    });
  }
}
