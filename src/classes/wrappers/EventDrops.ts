declare var require: any;
var d3 = require('d3/build/d3.node.js');
var eventDrops = require('event-drops/dist/index.js');

const chart = eventDrops({ d3 });

export class EventDrops {
    public tooltip: any;
    constructor() {

    }

    parse_data() {

    }

    // Example display: September 4 1986 8:30 PM
    humanizeDate = (date) => {
        const monthNames = [
            'Ene.',
            'Feb.',
            'Mar',
            'Abr.',
            'May',
            'Jun',
            'Jul.',
            'Ago.',
            'Sep.',
            'Oct.',
            'Nov.',
            'Dic.',
        ];

        return `
            ${monthNames[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}
            ${date.getHours()}:${date.getMinutes()}
        `;
    }

    render(repositories: any, _placeholder: string) {

        // const repositories = require('./data.json');
        const numberCommitsContainer = document.getElementById(_placeholder + '_numberCommits');
        const zoomStart = document.getElementById(_placeholder + '_zoomStart');
        const zoomEnd = document.getElementById(_placeholder + '_zoomEnd');

        const repositoriesData = repositories.map(repository => ({
            name: repository.name,
            data: repository.commits,
        }));


        const updateCommitsInformation = chart => {
            const filteredData = chart
                .filteredData()
                .reduce((total, repo) => total.concat(repo.data), []);

            numberCommitsContainer.textContent = (5 * filteredData.length).toString();
            zoomStart.textContent = this.humanizeDate(chart.scale().domain()[0]);
            zoomEnd.textContent = this.humanizeDate(chart.scale().domain()[1]);
        };

        const tooltip = d3
            .select('body')
            .append('div')
            .classed('tooltip', true)
            .style('opacity', 0);

        const chart = eventDrops({
            d3,
            metaballs: {
                blurDeviation: 5,
            },
            zoom: {
                onZoomEnd: () => updateCommitsInformation(chart),
            },
            drop: {
                radius: 3,
                date: d => new Date(d.date),
                onMouseOver: commit => {
                    tooltip
                        .transition()
                        .duration(200)
                        .style('opacity', 1);

                    tooltip
                        .html(`
                        <div class="col-md-8">
                            <div class="box">
                                <div class="box-body" style="background:#FFF">

                                        <li class="d-flex justify-content-start g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-10">
                                            <div class="g-mt-2">
                                                <img class="g-width-50 g-height-50 rounded-circle mCS_img_loaded" src="${commit.author.image}" alt="Image Description">
                                            </div>
                                            <div class="align-self-center g-px-10">
                                                <h5 class="h6 g-font-weight-600 g-color-black g-mb-3">
                                                    <span class="g-mr-5">@${commit.author.name}</span>
                                                    <small class="g-font-size-12 g-color-blue">${this.humanizeDate( new Date(commit.date))}</small>
                                                </h5>
                                                <p class="m-0">${commit.message}</p>
                                            </div>
                                        </li>

                                </div>
                            </div>
                        </div>
                        `)
                        .style('left', `${d3.event.pageX - 10}px`)
                        .style('top', `${d3.event.pageY + 10}px`);
                },
                onMouseOut: () => {
                    tooltip
                        .transition()
                        .duration(500)
                        .style('opacity', 0);
                },
            },
            label: {
                padding: 20,
                text: d => `${d.name} (${10 * d.data.length})`,
                width: 150,
            },
            margin: {
                top: 40,
                right: 20,
                bottom: 40,
                left: 10,
            },
            range: {
                start: new Date(new Date().getTime() - 4 * 60 * 60 * 1000),
                end: new Date().getTime() - 0 * 60 * 60 * 1000
            }
        });

        d3
            .select('#_historical_sentiment_chart')
            .data([repositoriesData])
            .call(chart);
        // console.log('EvenDrop');
        // console.log(repositoriesData);

    }

}
