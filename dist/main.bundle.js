webpackJsonp(["main"],{

/***/ "../../../../../src/$$_lazy_route_resource lazy recursive":
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "../../../../../src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "../../../../../src/app/app.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".box-close {\n    border: 1px solid rgba(0, 0, 0, 0.3);\n    background-color: white;\n    -webkit-box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0 rgba(0, 0, 0, 0.11);\n            box-shadow: 0 1px 1px 0 rgba(0, 0, 0, 0.1), 0 1px 1px 0 rgba(0, 0, 0, 0.11);\n    /* box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19); */\n}\n\n.sidebar-right-wrapper {\n    z-index: 1000;\n    padding-right: 10%;\n    position: fixed;\n    right: 250px;\n    width: 30%;\n    height: 80%;\n    margin-right: -20%;\n    /* overflow-y: auto; */\n    /* background: #000; */\n    -webkit-transition: all 0.5s ease;\n    transition: all 0.5s ease;\n}\n\n.sidebar-right-nav {\n    position: absolute;\n    top: 0;\n    width: 250px;\n    margin: 0;\n    padding: 0;\n    list-style: none;\n}\n\n.sidebar-right-nav li {\n    text-indent: 20px;\n    line-height: 40px;\n}\n\n.sidebar-right-nav li a {\n    display: block;\n    text-decoration: none;\n    color: #999999;\n}\n\n.sidebar-right-nav li a:hover {\n    text-decoration: none;\n    color: #fff;\n    background: rgba(255, 255, 255, 0.2);\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/app.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- navbar -->\n<app-navbar class=\"fixed-top\"></app-navbar>\n<br>\n<br>\n\n\n<mat-drawer-container>\n    <mat-drawer-content>\n        <br>\n        <div class=\"container-fluid\">\n            <!-- <app-main-topics-panel></app-main-topics-panel> -->\n            <!-- <div class=\"\"> -->\n            <div class=\"col-md-12\">\n                <app-main-panel [_keywords]=\"tweetService._current_keywords\"></app-main-panel>\n                <br>\n                <!--  -->\n                <!-- <app-left-panel class=\"col-md-4 col-sm-4 col-xs-4\"></app-left-panel> -->\n                <!--  -->\n                <!-- <app-right-panel class=\"col-md-2 col-sm-2 col-xs-2\"></app-right-panel> -->\n            </div>\n            <!-- </div> -->\n            <hr>\n            <div class=\"col-md-12\">\n                <!-- <h3>Analisis Individual</h3>\n                <p class=\"\">\n                    Selecciona uno de los candidatos para obtener informacion detallada a cerca de su impacto y percepcion publica.\n                </p> -->\n                <div class=\"row\">\n                    <div class=\"col-md-12\">\n                        <app-quick-access [_keywords]=\"tweetService._current_keywords\"></app-quick-access>\n                        <br>\n                    </div>\n\n                    <!-- <div class=\"col-md-12\">\n                        <app-conversation [_keywords]=\"tweetService._current_keywords\"></app-conversation>\n                        <br>\n                    </div> -->\n\n                </div>\n            </div>\n        </div>\n\n        <div class=\"col-md-12\">\n            <br>\n            <mat-card class=\"example-card\">\n                <mat-card-content>\n                    <div class=\"fb-comments\" data-href=\"http://www.notybook.com/\" data-numposts=\"5\"></div>\n                </mat-card-content>\n            </mat-card>\n        </div>\n    </mat-drawer-content>\n</mat-drawer-container>\n\n<app-footer></app-footer>\n"

/***/ }),

/***/ "../../../../../src/app/app.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(tweetService) {
        this.tweetService = tweetService;
        this.tweetService.current_keywords()
            .subscribe(function (res) {
            // console.log(this.tweetService._current_keywords);
        });
    }
    AppComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-root',
            template: __webpack_require__("../../../../../src/app/app.component.html"),
            styles: [__webpack_require__("../../../../../src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_tweet_service__["a" /* TweetService */]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "../../../../../src/app/app.module.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__("../../../platform-browser/esm5/platform-browser.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__("../../../../../src/app/app.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__ = __webpack_require__("../../../../@ng-bootstrap/ng-bootstrap/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__left_panel_left_panel_component__ = __webpack_require__("../../../../../src/app/left-panel/left-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__main_panel_main_panel_component__ = __webpack_require__("../../../../../src/app/main-panel/main-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__navbar_navbar_component__ = __webpack_require__("../../../../../src/app/navbar/navbar.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__main_topics_panel_main_topics_panel_component__ = __webpack_require__("../../../../../src/app/main-topics-panel/main-topics-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__footer_footer_component__ = __webpack_require__("../../../../../src/app/footer/footer.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__main_panel_historical_sentiment_historical_sentiment_component__ = __webpack_require__("../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__main_panel_topics_topics_component__ = __webpack_require__("../../../../../src/app/main-panel/topics/topics.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__main_panel_user_volume_user_volume_component__ = __webpack_require__("../../../../../src/app/main-panel/user-volume/user-volume.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__main_panel_most_hated_tweet_most_hated_tweet_component__ = __webpack_require__("../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__main_panel_most_loved_tweet_most_loved_tweet_component__ = __webpack_require__("../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__main_panel_daily_sentiment_daily_sentiment_component__ = __webpack_require__("../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__left_panel_contact_contact_component__ = __webpack_require__("../../../../../src/app/left-panel/contact/contact.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__services_contact_service__ = __webpack_require__("../../../../../src/services/contact.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular_star_rating__ = __webpack_require__("../../../../angular-star-rating/dist/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19_angular_star_rating___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_19_angular_star_rating__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20_ng4_twitter_timeline_lib_index__ = __webpack_require__("../../../../ng4-twitter-timeline/lib/index.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__right_panel_right_panel_component__ = __webpack_require__("../../../../../src/app/right-panel/right-panel.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__right_panel_mysentiment_mysentiment_component__ = __webpack_require__("../../../../../src/app/right-panel/mysentiment/mysentiment.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__right_panel_mytags_mytags_component__ = __webpack_require__("../../../../../src/app/right-panel/mytags/mytags.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__angular_platform_browser_animations__ = __webpack_require__("../../../platform-browser/esm5/animations.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__main_panel_historical_sentiment_summary_summary_component__ = __webpack_require__("../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__conversation_conversation_component__ = __webpack_require__("../../../../../src/app/conversation/conversation.component.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__angular_material_tabs__ = __webpack_require__("../../../material/esm5/tabs.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__angular_material_card__ = __webpack_require__("../../../material/esm5/card.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__angular_material__ = __webpack_require__("../../../material/esm5/material.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__angular_material_button__ = __webpack_require__("../../../material/esm5/button.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__angular_material_chips__ = __webpack_require__("../../../material/esm5/chips.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__angular_material_table__ = __webpack_require__("../../../material/esm5/table.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__angular_material_sidenav__ = __webpack_require__("../../../material/esm5/sidenav.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__angular_material_grid_list__ = __webpack_require__("../../../material/esm5/grid-list.es5.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__quick_access_quick_access_component__ = __webpack_require__("../../../../../src/app/quick-access/quick-access.component.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_5__left_panel_left_panel_component__["a" /* LeftPanelComponent */],
                __WEBPACK_IMPORTED_MODULE_6__main_panel_main_panel_component__["a" /* MainPanelComponent */],
                __WEBPACK_IMPORTED_MODULE_7__navbar_navbar_component__["a" /* NavbarComponent */],
                __WEBPACK_IMPORTED_MODULE_8__main_topics_panel_main_topics_panel_component__["a" /* MainTopicsPanelComponent */],
                __WEBPACK_IMPORTED_MODULE_9__footer_footer_component__["a" /* FooterComponent */],
                __WEBPACK_IMPORTED_MODULE_10__main_panel_historical_sentiment_historical_sentiment_component__["a" /* HistoricalSentimentComponent */],
                __WEBPACK_IMPORTED_MODULE_11__main_panel_topics_topics_component__["a" /* TopicsComponent */],
                __WEBPACK_IMPORTED_MODULE_12__main_panel_user_volume_user_volume_component__["a" /* UserVolumeComponent */],
                __WEBPACK_IMPORTED_MODULE_13__main_panel_most_hated_tweet_most_hated_tweet_component__["a" /* MostHatedTweetComponent */],
                __WEBPACK_IMPORTED_MODULE_14__main_panel_most_loved_tweet_most_loved_tweet_component__["a" /* MostLovedTweetComponent */],
                __WEBPACK_IMPORTED_MODULE_15__main_panel_daily_sentiment_daily_sentiment_component__["a" /* DailySentimentComponent */],
                __WEBPACK_IMPORTED_MODULE_21__right_panel_right_panel_component__["a" /* RightPanelComponent */],
                __WEBPACK_IMPORTED_MODULE_22__right_panel_mysentiment_mysentiment_component__["a" /* MysentimentComponent */],
                __WEBPACK_IMPORTED_MODULE_23__right_panel_mytags_mytags_component__["a" /* MytagsComponent */],
                __WEBPACK_IMPORTED_MODULE_16__left_panel_contact_contact_component__["a" /* ContactComponent */],
                __WEBPACK_IMPORTED_MODULE_25__main_panel_historical_sentiment_summary_summary_component__["a" /* SummaryComponent */],
                __WEBPACK_IMPORTED_MODULE_26__conversation_conversation_component__["a" /* ConversationComponent */],
                __WEBPACK_IMPORTED_MODULE_35__quick_access_quick_access_component__["a" /* QuickAccessComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_4__ng_bootstrap_ng_bootstrap__["a" /* NgbModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_19_angular_star_rating__["StarRatingModule"].forRoot(),
                __WEBPACK_IMPORTED_MODULE_20_ng4_twitter_timeline_lib_index__["b" /* Ng4TwitterTimelineModule */],
                __WEBPACK_IMPORTED_MODULE_24__angular_platform_browser_animations__["a" /* BrowserAnimationsModule */],
                __WEBPACK_IMPORTED_MODULE_27__angular_material_tabs__["a" /* MatTabsModule */],
                __WEBPACK_IMPORTED_MODULE_28__angular_material_card__["a" /* MatCardModule */],
                __WEBPACK_IMPORTED_MODULE_29__angular_material__["a" /* MatIconModule */],
                __WEBPACK_IMPORTED_MODULE_30__angular_material_button__["a" /* MatButtonModule */],
                __WEBPACK_IMPORTED_MODULE_31__angular_material_chips__["a" /* MatChipsModule */],
                __WEBPACK_IMPORTED_MODULE_32__angular_material_table__["a" /* MatTableModule */],
                __WEBPACK_IMPORTED_MODULE_33__angular_material_sidenav__["a" /* MatSidenavModule */],
                __WEBPACK_IMPORTED_MODULE_34__angular_material_grid_list__["a" /* MatGridListModule */]
            ],
            providers: [__WEBPACK_IMPORTED_MODULE_17__services_tweet_service__["a" /* TweetService */], __WEBPACK_IMPORTED_MODULE_18__services_contact_service__["a" /* ContactService */]],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* AppComponent */]],
            exports: [__WEBPACK_IMPORTED_MODULE_25__main_panel_historical_sentiment_summary_summary_component__["a" /* SummaryComponent */]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "../../../../../src/app/conversation/conversation.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/conversation/conversation.component.html":
/***/ (function(module, exports) {

module.exports = "<mat-card class=\"example-card\">\n    <mat-card-content>\n\n        <mat-card *ngFor=\"let keyword of _keywords\" class=\"example-card\">\n            <mat-card-content>\n                {{keyword}}\n            </mat-card-content>\n        </mat-card>\n\n\n    </mat-card-content>\n</mat-card>"

/***/ }),

/***/ "../../../../../src/app/conversation/conversation.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConversationComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ConversationComponent = /** @class */ (function () {
    function ConversationComponent(tweetService) {
        this.tweetService = tweetService;
    }
    ConversationComponent.prototype.ngOnInit = function () { };
    ConversationComponent.prototype.ngOnChanges = function (changes) {
        // historical data
        if (changes['_keywords']) {
            if (this._keywords) {
                // this.retrieve_historical_data(this._keywords);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], ConversationComponent.prototype, "_keywords", void 0);
    ConversationComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-conversation',
            template: __webpack_require__("../../../../../src/app/conversation/conversation.component.html"),
            styles: [__webpack_require__("../../../../../src/app/conversation/conversation.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_tweet_service__["a" /* TweetService */]])
    ], ConversationComponent);
    return ConversationComponent;
}());



/***/ }),

/***/ "../../../../../src/app/footer/footer.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footer {\n    position: relative;\n    bottom: 0;\n    width: 100%;\n    height: 60px;\n    line-height: 60px;\n    background-color: #008cba;\n    color: white;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.html":
/***/ (function(module, exports) {

module.exports = "<br>\n<footer class=\"footer\">\n    <div class=\"container\">\n        <span class=\" \">Umayux-labs - Copyright © Todos los Derechos Reservados 2018</span>\n    </div>\n</footer>"

/***/ }),

/***/ "../../../../../src/app/footer/footer.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FooterComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var FooterComponent = /** @class */ (function () {
    function FooterComponent() {
    }
    FooterComponent.prototype.ngOnInit = function () {
    };
    FooterComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-footer',
            template: __webpack_require__("../../../../../src/app/footer/footer.component.html"),
            styles: [__webpack_require__("../../../../../src/app/footer/footer.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], FooterComponent);
    return FooterComponent;
}());



/***/ }),

/***/ "../../../../../src/app/left-panel/contact/contact.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n}\n\n.example-full-width {\n  width: 100%;\n}\n\n.example-container {\n  display: -webkit-box;\n  display: -ms-flexbox;\n  display: flex;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n      -ms-flex-direction: column;\n          flex-direction: column;\n}\n\n.example-container > * {\n  width: 100%;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/left-panel/contact/contact.component.html":
/***/ (function(module, exports) {

module.exports = "<h1>Contáctanos</h1>\n        <p class=\"lead\">¿Tienes una pregunta o necesitas más información?</p>\n<!--<form action=\"http://18.221.42.69:57052/retrieve/keywords\" method=\"get\">-->\n  <div class=\"form-row\">\n    <div class=\"col-md-12 mb-3\">\n      <label for=\"validationServer01\">Nombre</label>\n      <input #input_name type=\"text\" class=\"form-control is-valid\" id=\"validationServer01\" placeholder=\"Nombre\" value=\"\" required>\n      <div class=\"valid-feedback\">\n        Ingrese su nombre!\n      </div>\n    </div>\n  </div>\n<div class=\"form-row\">\n    <div class=\"col-md-12 mb-3\">\n      <label for=\"validationServerUsername\">Email</label>\n      <div class=\"input-group\">\n        <div class=\"input-group-prepend\">\n          <span class=\"input-group-text\" id=\"inputGroupPrepend3\">@</span>\n        </div>\n        <input #input_email type=\"text\" class=\"form-control is-invalid\" id=\"validationServerUsername\" placeholder=\"Email\" aria-describedby=\"inputGroupPrepend3\" required>\n        <div class=\"invalid-feedback\">\n          Por favor, ingrese su email.\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"form-row\">\n    <div class=\"col-md-12 mb-3\">\n      <label for=\"validationServer03\">Mensaje</label>\n      <!--<input type=\"text\" class=\"form-control is-invalid\" id=\"validationServer03\" placeholder=\"Mensaje\" required>-->\n      <textarea #input_message class=\"form-control is-invalid\" id=\"exampleFormControlTextarea1\" placeholder=\"Mensaje\" required rows=\"3\"></textarea>\n      <div class=\"invalid-feedback\">\n        Por favor, ingrese su inquietud.\n      </div>\n    </div>\n    \n  </div>\n  \n  <button class=\"btn btn-primary btn-lg btn-block\" type=\"submit\" (click)=\"send_email_click(input_name.value, input_email.value, input_message.value)\">Contáctenos</button>\n<p>{{success}}</p>\n<!--</form>-->\n\n\n"

/***/ }),

/***/ "../../../../../src/app/left-panel/contact/contact.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_contact_service__ = __webpack_require__("../../../../../src/services/contact.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// services

var ContactComponent = /** @class */ (function () {
    function ContactComponent(contactService, ref) {
        this.contactService = contactService;
        this.ref = ref;
    }
    ContactComponent.prototype.ngOnInit = function () {
        this.success = '';
    };
    ContactComponent.prototype.send_email_click = function (names, email, message) {
        var _this = this;
        this.success = 'Enviando...';
        this.contactService.send_email(names, email, message).subscribe(function (res) {
            _this.success = 'Su mensaje ha sido enviado';
            setTimeout(function () {
                _this.success = '';
            }, 2000);
        });
    };
    ContactComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-contact',
            template: __webpack_require__("../../../../../src/app/left-panel/contact/contact.component.html"),
            styles: [__webpack_require__("../../../../../src/app/left-panel/contact/contact.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_contact_service__["a" /* ContactService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], ContactComponent);
    return ContactComponent;
}());



/***/ }),

/***/ "../../../../../src/app/left-panel/left-panel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/left-panel/left-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"card\">\n\n    <!-- <a class=\"twitter-timeline\" data-lang=\"en\" data-width=\"100%\" data-height=\"800\" data-theme=\"light\" data-link-color=\"#981CEB\" href=\"https://twitter.com/notybookcity?ref_src=twsrc%5Etfw\"></a> -->\n    <!-- <script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script> -->\n\n    <!-- <ng4-twitter-timeline></ng4-twitter-timeline> -->\n\n\n    <div *ngIf=\"render\">\n        <h3 class=\"card-header\">Inteligencia Artificial</h3>\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">Especial #presidenciales</h5>\n            <h6 class=\"card-subtitle text-muted\">Colombia 2018</h6>\n            <p>{{ spanishDate(_date_now) }}</p>\n        </div>\n        <img style=\"height: 200px; width: 100%; display: block;\" src=\"https://pbs.twimg.com/media/DVzedSyVMAAyPKQ.jpg\" alt=\"\">\n        <div [style.text-align] = '\"justify\"' class=\"card-body\">\n            <p class=\"card-text\">\n                Bienvenido a @notybook, un portal especializado en el analisis de datos de actualidad en redes sociales en tiempo real. Usando los ultimos avances en inteligencia artificial, nuestros modelos de procesamiento de lenguaje natural ofrecen una alternativa\n                al analisis y compresion de la perception publica.\n            </p>\n            <p>\n                Este portal esta dedicado al analisis de datos de los candidatos presidenciales para el termino 2018-2022 en Colombia.\n            </p>\n\n            <p>\n                Si deseas saber mas sobre analisis de datos (data analytics), por favor, da click aqui para ponerte en contacto con nuesto nuestros asesores.\n            </p>\n            <app-contact> </app-contact>\n\n            <h1>Síganos en</h1>\n            \n                <div class=\"fb-like\" data-href=\"https://www.facebook.com/notybookcity/\" data-layout=\"button_count\" data-action=\"like\" data-size=\"small\" data-show-faces=\"true\" data-share=\"false\"></div> \n                <div class=\"fb-share-button\" data-href=\"http://www.notybook.com/\" data-layout=\"button\" data-size=\"small\" data-mobile-iframe=\"true\"><a target=\"_blank\" href=\"https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.notybook.com%2F&amp;src=sdkpreparse\" class=\"fb-xfbml-parse-ignore\">Compartir</a></div>\n                \n            \n            \n        </div>\n        <!-- <ul class=\"list-group list-group-flush\">\n        </ul> -->\n        <!-- <br>\n        <div class=\"card-footer\">\n\n        </div> -->\n\n        \n\n    </div>\n\n    <div *ngIf=\"!render\">\n        <div class=\"timeline-wrapper\">\n            <div class=\"timeline-item\">\n                <div class=\"animated-background\">\n                    <div class=\"background-masker header-top\"></div>\n                    <div class=\"background-masker header-left\"></div>\n                    <div class=\"background-masker header-right\"></div>\n                    <div class=\"background-masker header-bottom\"></div>\n                    <div class=\"background-masker subheader-left\"></div>\n                    <div class=\"background-masker subheader-right\"></div>\n                    <div class=\"background-masker subheader-bottom\"></div>\n                    <div class=\"background-masker content-top\"></div>\n                    <div class=\"background-masker content-first-end\"></div>\n                    <div class=\"background-masker content-second-line\"></div>\n                    <div class=\"background-masker content-second-end\"></div>\n                    <div class=\"background-masker content-third-line\"></div>\n                    <div class=\"background-masker content-third-end\"></div>\n                </div>\n            </div>\n        </div>\n    </div>\n\n\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/left-panel/left-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LeftPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LeftPanelComponent = /** @class */ (function () {
    function LeftPanelComponent() {
    }
    LeftPanelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.render = false;
        setTimeout(function () { _this.render = true; }, 500);
        this._date_now = new Date();
    };
    LeftPanelComponent.prototype.spanishDate = function (d) {
        var weekday = ['Domingo', "Lunes", 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
        var monthname = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
        return weekday[d.getDay()] + ' ' + d.getDate() + ' de ' + monthname[d.getMonth()] + ' de ' + d.getFullYear() + ' - ' + d.getHours() + ':' + d.getMinutes();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", String)
    ], LeftPanelComponent.prototype, "_names", void 0);
    LeftPanelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-left-panel',
            template: __webpack_require__("../../../../../src/app/left-panel/left-panel.component.html"),
            styles: [__webpack_require__("../../../../../src/app/left-panel/left-panel.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], LeftPanelComponent);
    return LeftPanelComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.html":
/***/ (function(module, exports) {

module.exports = "<hr>\n<div class=\"row no-gutters\">\n    <div class=\"col-md-12 no-gutters\">\n        <h2 class=\"g-font-weight-600\"> Reaccion de usuarios a ultimo comentario de los candidatos en twitter</h2>\n        <p>Esta seccion te muestra el volumen de comentarios positivos, negativos, neutrales, y el impacto de el ultimo comentario hecho por cada uno de los candidatos. Esta informacion esta monitoreada en tiempo real y se actualiza automaticamente cada\n            30 segundos. Lo que ves en esta seccion es lo que esta sucediendo en este preciso momento</p>\n    </div>\n    <br>\n\n    <div *ngIf=\"_render\" class=\"row\">\n        <ul *ngFor=\"let profile of profiles\" class=\"no-gutters\" style=\"padding: 0\">\n            <li class=\"bg-white d-flex justify-content-start g-brd-around g-brd-gray-light-v1 g-pa-20 g-mb-12\">\n                <div class=\"g-mt-2\">\n                    <img class=\"g-width-50 g-height-50 rounded-circle mCS_img_loaded\" [src]=\"profile.status[0].data[0].user.profile_image_url\" alt=\"Image Description\">\n                </div>\n                <div class=\"align-self-center g-px-10\">\n                    <h5 class=\"h6 g-font-weight-600 g-color-black g-mb-3\">\n                        <span class=\"g-mr-5\">{{ profile.status[0].data[0].user.name }} (@{{ profile.status[0].data[0].user.screen_name }}) </span>\n                    </h5>\n                    <span>\n            <strong> Creado: </strong> {{ profile.status[0].data[0].created_at }} </span>\n                    <p>\n                        <strong>Indice de popularidad:</strong>\n                        <strong>{{ (1000*60*(profile.sentiment[0].data[0].n_positive+profile.sentiment[0].data[0].n_negative + profile.sentiment[0].data[0].n_neutral)/(current_time\n              - profile.status[0].data[0].timestamp_ms)).toFixed(2) }} </strong> (comentarios/minuto) </p>\n                    <!-- <hr> -->\n                    <span>\n            <strong>Comentario:</strong>\n          </span>\n                    <p>\n                        <span *ngIf=\"profile.status[0].data[0].extended_tweet; else short_text\"> {{ profile.status[0].data[0].extended_tweet.full_text }} </span>\n                    </p>\n                    <p>\n                        <span>\n              <ng-template #short_text> {{ profile.status[0].data[0].text }} </ng-template>\n            </span>\n                    </p>\n\n                    <span>\n            <strong>Contexto del comentario:</strong>\n          </span>\n                    <br>\n                    <span>\n            <i class=\"fa fa-smile-o icon-blue\"></i> {{ (100*profile.status[0].data[0].sentiment.positive).toFixed(1) }}% positivo</span>\n                    <span>\n            <i class=\"fa fa-frown-o icon-red\"></i> {{ (100*profile.status[0].data[0].sentiment.negative).toFixed(1) }}% negativo </span>\n                    <br>\n                    <br>\n                    <span>\n            <strong>Categorias del comentario:</strong>\n          </span>\n                    <br>\n                    <p>\n                        <span *ngFor=\"let tag of profile.status[0].data[0].tags.prediction\" [style.font-weight]=\" 200+1000*tag.score \" class=\"g-bg-gray-light-v4 g-color-main g-rounded-20 g-px-10\">\n              {{ tag.tag }}\n            </span>\n                    </p>\n                    <hr>\n                    <span>\n            <strong>Reacciones a este comentario: </strong>\n          </span>\n                    <br>\n                    <span>\n            <i class=\"fa fa-smile-o icon-blue\"></i> {{ profile.sentiment[0].data[0].n_positive }} comentarios positivos</span>\n                    <span>\n            <i class=\"fa fa-frown-o icon-red\"></i> {{ profile.sentiment[0].data[0].n_negative }} comentarios negativos </span>\n                    <span>\n            <i class=\"fa fa-meh-o icon-black\"></i> {{ profile.sentiment[0].data[0].n_neutral }} comentarios neutrales </span>\n                    <!-- positive negative panel -->\n                    <!-- <div class=\"js-hr-progress-bar progress g-bg-black-opacity-0_1 rounded-0 g-mb-5\"> -->\n                    <!-- <div class=\"js-hr-progress-bar-indicator progress-bar g-bg-red u-progress-bar--xs\" role=\"progressbar\" [style.width]=\"'profile.sentiment[0].data[0].n_positive'\" aria-valuenow=\"68\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div> -->\n                    <!-- </div> -->\n                    <br>\n                    <br>\n                    <span>\n            <strong>Principales topicos encontrados en las respuestas a este comentario: </strong>\n          </span>\n                    <br>\n                    <span *ngFor=\"let tag of profile.tags[0].data[0]\" [style.font-weight]=\" 300*tag.score \" class=\"g-bg-gray-light-v4 g-color-main g-rounded-20 g-px-10\">\n          {{ tag.tag }} </span>\n\n                    <!-- <div class=\"js-hr-progress-bar progress g-bg-black-opacity-0_1 rounded-0 g-mb-5\"> -->\n                    <!-- <div class=\"js-hr-progress-bar-indicator progress-bar g-bg-cyan u-progress-bar--xs\" role=\"progressbar\" style=\"width: 68%\" [aria-valuenow]=\"post[0].data[0].sentiment.positive.toFixed(2)*100\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div> -->\n                    <!-- </div> -->\n                    <!-- <p class=\"m-0\">Percepcion positiva</p> -->\n                    <hr>\n                    <div class=\"col-md-12\">\n                        <div class=\"row\">\n                            <div clas=\"col\">\n                                <strong>Como calificas este comentario?</strong>\n                            </div>\n                            <div class=\"col\">\n                                <star-rating-comp *ngIf=\"profile.render\" [starType]=\"'svg'\" [rating]=\"0\"></star-rating-comp>\n                            </div>\n\n                        </div>\n                    </div>\n                </div>\n\n            </li>\n        </ul>\n\n    </div>\n\n    <hr class=\"col-md-12\">\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DailySentimentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_wrappers_LdaModel__ = __webpack_require__("../../../../../src/classes/wrappers/LdaModel.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// services


var DailySentimentComponent = /** @class */ (function () {
    function DailySentimentComponent(tweetService) {
        this.tweetService = tweetService;
    }
    DailySentimentComponent.prototype.ngOnInit = function () {
        this.profiles = [];
        this._render = false;
        this.Math = Math;
        this.current_time = new Date().getTime();
        this._interval_id = null;
        this.rating = { starting: 5 };
        this.lda = new __WEBPACK_IMPORTED_MODULE_2__classes_wrappers_LdaModel__["a" /* LDA */]();
    };
    DailySentimentComponent.prototype.ngOnChanges = function (changes) {
        var _this = this;
        // historical data
        if (changes['_keywords']) {
            if (this._keywords) {
                // execute historical data
                this.retrieve_historical_data(this._keywords);
                // this.profiles = [];
                // execute each 10 seconds make a request
                this._interval_id = setInterval(function () {
                    _this.current_time = new Date().getTime();
                    _this.retrieve_historical_data(_this._keywords);
                    // this.profiles = [];
                }, 30000);
            }
        }
    };
    DailySentimentComponent.prototype.ngOnDestroy = function () {
        if (this._interval_id) {
            clearInterval(this._interval_id);
        }
    };
    DailySentimentComponent.prototype.update_profile = function (profiles, res) {
        var i = res.status[0].data[0].user.name;
        profiles[i].status[0].data[0].user.profile_image_url =
            res.status[0].data[0].user.profile_image_url;
        profiles[i].sentiment[0].data[0].n_positive =
            res.sentiment[0].data[0].n_positive;
        profiles[i].sentiment[0].data[0].n_negative =
            res.sentiment[0].data[0].n_negative;
        profiles[i].sentiment[0].data[0].n_neutral =
            res.sentiment[0].data[0].n_neutral;
        profiles[i].status[0].data[0].user.name = res.status[0].data[0].user.name;
        profiles[i].status[0].data[0].user.screen_name =
            res.status[0].data[0].user.screen_name;
        profiles[i].status[0].data[0].created_at = res.status[0].data[0].created_at;
        profiles[i].status[0].data[0].timestamp_ms =
            res.status[0].data[0].timestamp_ms;
        try {
            profiles[i].status[0].data[0].extended_tweet.full_text =
                res.status[0].data[0].extended_tweet.full_text;
        }
        catch (_a) {
            profiles[i].status[0].data[0].text = res.status[0].data[0].text;
        }
        profiles[i].tags[0].data[0] = res.tags[0].data[0];
    };
    DailySentimentComponent.prototype.retrieve_historical_data = function (keywords) {
        var _this = this;
        keywords.forEach(function (e, i) {
            // traverse the keyword list and get the data for each one of the keywords,
            // then add it to the historical-data variable.
            _this.tweetService.tweet_status(e).subscribe(function (res) {
                // console.log(res);
                // const _sentiment = res.sentiment[0].data[0];
                // const _status = res.status[0].data[0];
                // const _tags = res.sentiment[0].data[0];
                if (_this.profiles.length === keywords.length) {
                    _this.profiles[i] = res;
                    // res.render = true;
                    // this.update_profile(this.profiles, res);
                }
                else {
                    // res.render = true;
                    // setTimeout(() => {
                    _this.profiles.push(res);
                    // this.profiles[_status.user.name] = res;
                    // }, 100);
                }
                // check if the historical_data variable has been populated by all the keyword's data
                if (_this.profiles.length === 1) {
                    // console.log(this.profiles);
                    _this._render = true;
                    // this.profiles[i].render = true;
                    // console.log(this.profiles);
                    // this.event_drops.render(this.historical_data, "_historical_sentiment_chart");
                }
            });
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], DailySentimentComponent.prototype, "_keywords", void 0);
    DailySentimentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-daily-sentiment',
            template: __webpack_require__("../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/daily-sentiment/daily-sentiment.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_tweet_service__["a" /* TweetService */]])
    ], DailySentimentComponent);
    return DailySentimentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".infos {\n    text-align: center;\n    margin-bottom: 1rem;\n}\n\n#zoomStart,\n#zoomEnd {\n    display: inline-block;\n    -webkit-font-feature-settings: \"tnum\";\n            font-feature-settings: \"tnum\";\n    font-variant-numeric: tabular-nums;\n}\n\n#zoomStart {\n    text-align: 'right';\n}\n\n#zoomEnd {\n    text-align: 'left';\n}\n\n.light {\n    color: #AAA;\n}\n\n.drop {\n    cursor: pointer;\n}\n\nfooter p {\n    text-align: center;\n    color: #888;\n    font-size: .8rem;\n}\n\n.tooltip {\n    position: absolute;\n    background: #fff;\n    border: 3px solid #e7e7e7;\n    border-radius: 1rem;\n    padding: .5rem 1rem;\n    width: 30rem;\n    line-height: 1.4rem;\n}\n\n.tooltip::before {\n    content: '';\n    display: block;\n    position: absolute;\n    top: -.65rem;\n    width: 1rem;\n    height: 1rem;\n    background: #fff;\n    border: 3px solid #e7e7e7;\n    border-width: 3px 0 0 3px;\n    -webkit-transform: rotate(45deg);\n            transform: rotate(45deg);\n    z-index: 1;\n}\n\n.tooltip.left::before {\n    left: 1.65rem;\n}\n\n.tooltip.right::before {\n    right: 1.65rem;\n}\n\n.tooltip .commit {\n    position: relative;\n    z-index: 2;\n    display: -webkit-box;\n    display: -ms-flexbox;\n    display: flex;\n    -webkit-box-orient: horizontal;\n    -webkit-box-direction: normal;\n        -ms-flex-direction: row;\n            flex-direction: row;\n    -webkit-box-align: center;\n        -ms-flex-align: center;\n            align-items: center;\n}\n\n.tooltip .avatar {\n    width: 5rem;\n    height: 5rem;\n    border-radius: .5rem;\n    margin-right: 1rem;\n    -webkit-box-flex: 0;\n        -ms-flex: 0 0 5rem;\n            flex: 0 0 5rem;\n}\n\n.tooltip .content {\n    -webkit-box-flex: 1;\n        -ms-flex: 1 0 0px;\n            flex: 1 0 0;\n}\n\n.tooltip h3 {\n    font-size: 1rem;\n}\n\n.tooltip p {\n    font-size: .9rem;\n    color: #777;\n}\n\n.no-gutters {\n    margin-right: 0;\n    margin-left: 0;\n    >.col,\n    >[class*=\"col-\"] {\n        padding-right: 0;\n        padding-left: 0;\n    }\n}\n\n.y-tick {\n    stroke: black;\n    fill: none;\n    stroke-width: 1px;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"col-md-12 no-gutters\" *ngIf=\"!render\" hidden>\n        <div class=\"timeline-wrapper col-md-12\">\n            <div class=\"timeline-item\">\n                <div class=\"animated-background\">\n                    <div class=\"background-masker header-top\"></div>\n                    <div class=\"background-masker header-left\"></div>\n                    <div class=\"background-masker header-right\"></div>\n                    <div class=\"background-masker header-bottom\"></div>\n                    <div class=\"background-masker subheader-left\"></div>\n                    <div class=\"background-masker subheader-right\"></div>\n                    <div class=\"background-masker subheader-bottom\"></div>\n                    <div class=\"background-masker content-top\"></div>\n                    <div class=\"background-masker content-first-end\"></div>\n                    <div class=\"background-masker content-second-line\"></div>\n                    <div class=\"background-masker content-second-end\"></div>\n                    <div class=\"background-masker content-third-line\"></div>\n                    <div class=\"background-masker content-third-end\"></div>\n                </div>\n            </div>\n        </div>\n    </div> -->\n\n<!-- <div class=\"col-md-12\">\n        <h1 class=\"g-font-weight-500\"> Presidenciales en Colombia</h1>\n\n    </div> -->\n\n<!-- <br> -->\n\n\n<!-- <div class=\"col-md-6 text-right\" hidden>\n        <strong class=\"badge badge-pill badge-light\" id=\"_historical_sentiment_chart_zoomStart\"></strong>\n        <span class=\"small\"></span>\n        <strong class=\"badge badge-pill badge-light\" id=\"_historical_sentiment_chart_zoomEnd\"></strong>\n    </div> -->\n\n<!-- <mat-grid-list cols=\"2\"> -->\n<!-- <mat-grid-tile> -->\n\n<div class=\"row\">\n    <div class=\"col-md-5\">\n        <mat-card class=\"example-card\">\n            <mat-card-content>\n                <h3 class=\"box-title\"> Analisis General</h3>\n                <p>\n                    La siguiente grafica muestra el volumen total de cada candidato para un periodo de 24 horas medido de acuerdo al numero de posts.\n                </p>\n                <app-summary [_keywords]=\"_keywords\"> </app-summary>\n            </mat-card-content>\n        </mat-card>\n    </div>\n    <!-- </mat-grid-tile> -->\n    <!-- <mat-grid-tile> -->\n    <div class=\"col-md-7\">\n        <mat-card class=\"example-card\">\n            <mat-card-content>\n                <h3 class=\"box-title\"> Evolucion de polaridad en el tiempo</h3>\n                <mat-tab-group (selectedTabChange)=\"render_favorability()\">\n                    <mat-tab>\n                        <ng-template mat-tab-label>\n                            <!-- <mat-icon>thumb_up</mat-icon> -->\n                            Comentarios Positivos\n                        </ng-template>\n                        <br>\n                        <p>La siguiente grafica muestra el volumen de los comentarios positivos en un rango de 24 horas</p>\n                        <!-- <div id=\"_historical_sentiment_chart\" class=\"col-md-12\"></div> -->\n                        <div id=\"_historical_positive_volume\" class=\"col-md-12\"></div>\n\n                    </mat-tab>\n                    <!-- <br> -->\n                    <mat-tab label=\"Favorabilidad\">\n                        <br>\n                        <p>La siguiente grafica muestra la *Favorabilidad neta en un rango de 24 horas</p>\n                        <p class=\"small\">*Favorabilidad neta se define como el %positivo - %negativo. Esta metrica describe la que tan positiva/negativa es la percepcion publica de cada candidato.</p>\n                        <div id=\"_historical_negative_volume\" class=\"col-md-12\"></div>\n                    </mat-tab>\n                </mat-tab-group>\n            </mat-card-content>\n        </mat-card>\n    </div>\n</div>\n<!-- </mat-grid-tile> -->\n<!-- </mat-grid-list> -->"

/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HistoricalSentimentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_EventDrops__ = __webpack_require__("../../../../../src/classes/wrappers/EventDrops.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// services


// import { LDA } from '../../../classes/wrappers/LdaModel';
var HistoricalSentimentComponent = /** @class */ (function () {
    // public lda: any;
    function HistoricalSentimentComponent(tweetService, ref) {
        this.tweetService = tweetService;
        this.ref = ref;
        this.event_drops = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_EventDrops__["a" /* EventDrops */]();
    }
    HistoricalSentimentComponent.prototype.ngOnInit = function () {
        this.historical_positive_data = [];
        this.historical_negative_data = [];
        this.render = false;
        this.highcharts = new __WEBPACK_IMPORTED_MODULE_3__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        // this.lda = new LDA();
        // console.log(
        //   this.lda.topics([
        //     'el dia de hoy todo fue bien',
        //     'feliz viernes de rumba',
        //     'gozar la vida en este dia'
        //   ])
        // );
        // LDA.topics()
    };
    HistoricalSentimentComponent.prototype.ngOnChanges = function (changes) {
        // historical data
        if (changes['_keywords']) {
            if (this._keywords) {
                this.retrieve_historical_data(this._keywords);
            }
        }
    };
    HistoricalSentimentComponent.prototype.retrieve_historical_data = function (keywords) {
        var _this = this;
        keywords.forEach(function (e, i) {
            // traverse the keyword list and get the data for each one of the keywords,
            // then add it to the historical-data variable.
            _this.tweetService.tweet_history_10h(e).subscribe(function (res) {
                // console.log(res);
                _this.historical_positive_data.push({
                    name: res.name,
                    data: res.data.map(function (e) { return e.n_positive; }),
                    categories: res.from.map(function (e) {
                        var dt = new Date(e);
                        return dt.toLocaleString('en-US', {
                            hour: 'numeric',
                            hour12: true
                        });
                    })
                });
                _this.historical_negative_data.push({
                    name: res.name,
                    data: res.data.map(function (e) {
                        return 100 *
                            (e.n_positive / (e.n_positive + e.n_negative + e.n_neutral) -
                                e.n_negative / (e.n_positive + e.n_negative + e.n_neutral));
                    }),
                    categories: res.from.map(function (e) {
                        var dt = new Date(e);
                        return dt.toLocaleString('en-US', {
                            hour: 'numeric',
                            hour12: true
                        });
                    })
                });
                // check if the historical_data variable has been populated by all the keyword's data
                if (_this.historical_positive_data.length === keywords.length) {
                    // this.event_drops.render(this.historical_data, '_historical_sentiment_chart');
                    _this.highcharts.area_spline('_historical_positive_volume', _this.historical_positive_data);
                    // console.log(this.historical_positive_data);
                    _this.render = true;
                    // console.log(this.historical_positive_data);
                }
            });
        });
    };
    HistoricalSentimentComponent.prototype.render_favorability = function () {
        // console.log('datadatadatadata');
        this.highcharts.area_spline('_historical_negative_volume', this.historical_negative_data);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], HistoricalSentimentComponent.prototype, "_keywords", void 0);
    HistoricalSentimentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-historical-sentiment',
            template: __webpack_require__("../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/historical-sentiment/historical-sentiment.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_tweet_service__["a" /* TweetService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], HistoricalSentimentComponent);
    return HistoricalSentimentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.html":
/***/ (function(module, exports) {

module.exports = "<div id=\"survey\">\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SummaryComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// services

var SummaryComponent = /** @class */ (function () {
    function SummaryComponent(tweetService, ref) {
        this.tweetService = tweetService;
        this.ref = ref;
    }
    SummaryComponent.prototype.ngOnChanges = function (changes) {
        // historical data
        if (changes['_keywords']) {
            if (this._keywords) {
                this.retrieve_historical_data(this._keywords);
            }
        }
    };
    SummaryComponent.prototype.ngOnInit = function () {
        this.historical_data = [];
        this.historical_data_sort = new Array();
        this.render = false;
    };
    SummaryComponent.prototype.retrieve_historical_data = function (keywords) {
        var _this = this;
        keywords.forEach(function (e, i) {
            // traverse the keyword list and get the data for each one of the keywords,
            // then add it to the historical-data variable.
            _this.tweetService.tweet_history_total(e).subscribe(function (res) {
                _this.historical_data.push(res);
                // check if the historical_data variable has been populated by all the keyword's data
                if (_this.historical_data.length === keywords.length) {
                    _this.set_values(_this.historical_data);
                    _this.render = true;
                }
            });
        });
    };
    SummaryComponent.prototype.set_values = function (historicalData) {
        var _this = this;
        this.countMessages = 0;
        historicalData.forEach(function (element) {
            _this.countMessages += element.value;
        });
        console.log(this.historical_data);
        var repositoriesData = historicalData.map(function (repository) { return ({
            name: repository.name,
            y: 100 * repository.value / _this.countMessages
        }); });
        this.data = [
            {
                name: 'encuesta',
                // colorByPoint: true,
                data: repositoriesData
            }
        ];
        this.highcharts = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        this.highcharts.barplot('survey', this.data);
        // console.log(this.data)
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], SummaryComponent.prototype, "_keywords", void 0);
    SummaryComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-summary',
            template: __webpack_require__("../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/historical-sentiment/summary/summary.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__services_tweet_service__["a" /* TweetService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["ChangeDetectorRef"]])
    ], SummaryComponent);
    return SummaryComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/main-panel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/main-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <div class=\"col-md-12 no-gutters\"> -->\n<app-historical-sentiment [_keywords]=\"_keywords\"></app-historical-sentiment>\n<div id=\"clustrmaps\" hidden>\n\n</div>\n<!-- </div> -->\n\n\n<!-- <div class=\"col-md-6 no-gutters\">\n    <div class=\"box box-solid\">\n        <div class=\"row no-gutters\">\n            <app-daily-sentiment [_keywords]=\"_keywords\" class=\"col-md-12\"></app-daily-sentiment>\n        </div>\n    </div>\n</div> -->"

/***/ }),

/***/ "../../../../../src/app/main-panel/main-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainPanelComponent = /** @class */ (function () {
    function MainPanelComponent() {
    }
    MainPanelComponent.prototype.ngOnInit = function () { };
    MainPanelComponent.prototype.ngOnChanges = function (changes) {
        if (changes['_keywords']) {
            // console.log(this._keywords);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], MainPanelComponent.prototype, "_keywords", void 0);
    MainPanelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-panel',
            template: __webpack_require__("../../../../../src/app/main-panel/main-panel.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/main-panel.component.css")]
        })
    ], MainPanelComponent);
    return MainPanelComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MostHatedTweetComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MostHatedTweetComponent = /** @class */ (function () {
    function MostHatedTweetComponent() {
    }
    MostHatedTweetComponent.prototype.ngOnInit = function () {
        this.tweet = {
            author: "German Vargas Lleras",
            text: "Vamos a implementar en el país el más ambicioso y amplio programa en materia de red terciaria. Atenderemos al menos 20 km de vías de la red terciaria por municipio. #MejorAgricultura"
        };
        this.data = [{
                name: 'Reacciones Positivas',
                colorByPoint: true,
                data: [{
                        name: 'Positivo',
                        y: 6.33,
                    }, {
                        name: 'Neutral',
                        y: 4.03,
                    }, {
                        name: 'Negativo',
                        y: 80.38
                    }]
            }];
        // 
        this.highcharts = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        this.highcharts.column("most-hated-pie-chart-01", this.data);
    };
    MostHatedTweetComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-most-hated-tweet',
            template: __webpack_require__("../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/most-hated-tweet/most-hated-tweet.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MostHatedTweetComponent);
    return MostHatedTweetComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.html":
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MostLovedTweetComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MostLovedTweetComponent = /** @class */ (function () {
    function MostLovedTweetComponent() {
    }
    MostLovedTweetComponent.prototype.ngOnInit = function () {
        this.tweet = {
            author: "Gustavo Petro",
            text: "La manifestación de Cartagena. El pueblo caribe con la Colombia Humana"
        };
        this.data = [{
                name: 'Reacciones Positivas',
                colorByPoint: true,
                data: [{
                        name: 'Positivo',
                        y: 56.33,
                    }, {
                        name: 'Neutral',
                        y: 24.03,
                    }, {
                        name: 'Negativo',
                        y: 10.38
                    }]
            }];
        // 
        this.highcharts = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        this.highcharts.column("most-loved-pie-chart-01", this.data);
    };
    MostLovedTweetComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-most-loved-tweet',
            template: __webpack_require__("../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/most-loved-tweet/most-loved-tweet.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MostLovedTweetComponent);
    return MostLovedTweetComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/topics/topics.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/topics/topics.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\" no-gutters\">\n\n    <div class=\"col-md-12\">\n        <h4> <i class=\"fa fa-users icon-green\"></i> Cuales han sido las reacciones de los usuarios?</h4>\n        <br>\n    </div>\n    <div class=\"row\">\n        <div style=\"height: 200px\" class=\"col-md-6\" id=\"topics-word-cloud-positive\"></div>\n        <div class=\"col-md-6\">\n            <h4>Topicos</h4>\n            <p>Esta seccion muestra los principales temas que despiertan la atencion en los colombianos. <strong>#Topic1</strong> es el topico mas mencionado por usuarios de twitter con un total de 000 usuarios activos. </p>\n        </div>\n    </div>\n\n    <div class=\"row\">\n        <div class=\"col-md-6\">\n            <h4>Topicos</h4>\n            <p>Esta seccion muestra los principales temas que despiertan la atencion en los colombianos. <strong>#Topic1</strong> es el topico mas mencionado por usuarios de twitter con un total de 000 usuarios activos. </p>\n        </div>\n        <div style=\"height: 200px\" class=\"col-md-6\" id=\"topics-word-cloud-negative\"></div>\n\n    </div>\n\n</div>"

/***/ }),

/***/ "../../../../../src/app/main-panel/topics/topics.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TopicsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

// import { WordCloud } from '../../../classes/wrappers/WordCloud'

var TopicsComponent = /** @class */ (function () {
    function TopicsComponent() {
    }
    TopicsComponent.prototype.ngOnInit = function () {
        this.data = [
            { name: 'politica', weight: 16 },
            { name: 'castrochavismo', weight: 2 },
            { name: 'corrupcion', weight: 12 },
            { name: 'parapolitica', weight: 10 },
            { name: 'farc', weight: 9 },
            { name: 'presidente', weight: 8 },
            { name: 'cambioradical', weight: 5 },
            { name: 'progresistas', weight: 3 },
            { name: 'venezuela', weight: 7 },
            { name: 'uribe', weight: 7 },
            { name: 'santos', weight: 7 },
            { name: 'verdad', weight: 7 },
        ];
        this.wordcloud = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        this.wordcloud.wordcloud('topics-word-cloud-positive', this.data);
        this.wordcloud.wordcloud('topics-word-cloud-negative', this.data);
    };
    TopicsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-topics',
            template: __webpack_require__("../../../../../src/app/main-panel/topics/topics.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/topics/topics.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], TopicsComponent);
    return TopicsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-panel/user-volume/user-volume.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-panel/user-volume/user-volume.component.html":
/***/ (function(module, exports) {

module.exports = "<h4> <i class=\"fa fa-cogs icon-green\"></i> Analisis individual de los candidatos presidenciales</h4>\n<p>Esta seccion te muestra el impacto que ha generado cada uno de los candidatos en las ultimas 24 horas y cual ha sido la evolucion del ese impacto a traves del tiempo. </p>\n\n\n<div class=\" media g-brd-around g-brd-gray-light-v4 g-brd-left-3 g-brd-pink-left rounded g-pa-20 g-mb-10\">\n    <div class=\"d-flex g-mt-2 g-mr-15\">\n        <img class=\"g-width-40 g-height-40 rounded-circle mCS_img_loaded\" src=\"https://pbs.twimg.com/profile_images/962471262747529216/Uo8lOkRJ_400x400.jpg\" alt=\"Image Description\">\n    </div>\n    <div class=\"media-body\">\n        <div class=\"d-flex justify-content-between\">\n            <h5 class=\"h6 g-font-weight-600 g-color-black\">Gustavo Petro</h5>\n            <span class=\"small text-nowrap g-color-pink\">16 min ago</span>\n        </div>\n        <p>Perfil Oficial del dirigente político progresista colombiano Gustavo Petro. Por una Colombia Humana con justicia social y en Paz.</p>\n\n        <span class=\"u-label u-label--sm g-bg-gray-light-v4 g-color-main g-rounded-20 g-px-10\">CSS</span>\n        <span class=\"u-label u-label--sm g-bg-gray-light-v4 g-color-main g-color-black g-rounded-20 g-px-10\">JavaScript</span>\n        <span class=\"u-label u-label--sm g-bg-gray-light-v4 g-color-main g-rounded-20 g-px-10\">Ruby</span>\n        <span class=\"u-label u-label--sm g-bg-gray-light-v4 g-color-main g-rounded-20 g-px-10\">ASP.NET</span>\n\n        <hr>\n\n        <div class=\"row \">\n            <div class=\"col-md-6 g-mb-10\">\n                <h6 class=\"g-mb-10\">Comentarios positivos <span class=\"float-right g-ml-10\">68%</span></h6>\n                <div class=\"js-hr-progress-bar progress g-bg-black-opacity-0_1 rounded-0 g-mb-5\">\n                    <div class=\"js-hr-progress-bar-indicator progress-bar g-bg-cyan u-progress-bar--xs\" role=\"progressbar\" style=\"width: 68%;\" aria-valuenow=\"68\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n                <small class=\"g-font-size-12\">11% mas que la semana pasada</small>\n            </div>\n\n            <div class=\"col-md-6 g-mb-10\">\n                <h6 class=\"g-mb-10\">Comentarios negativos <span class=\"float-right g-ml-10\">12%</span></h6>\n                <div class=\"js-hr-progress-bar progress g-bg-black-opacity-0_1 rounded-0 g-mb-5\">\n                    <div class=\"js-hr-progress-bar-indicator progress-bar g-bg-red u-progress-bar--xs\" role=\"progressbar\" style=\"width: 12%;\" aria-valuenow=\"68\" aria-valuemin=\"0\" aria-valuemax=\"100\"></div>\n                </div>\n                <small class=\"g-font-size-12\">10% menos que la semana pasada</small>\n            </div>\n\n        </div>\n\n        <div class=\"no-gutters\">\n            <br>\n            <h5> <strong> Evolucion de la percepcion publica en el tiempo </strong></h5>\n            <div id=\"perception-by-time\" class=\"col-md-12\"></div>\n        </div>\n\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/main-panel/user-volume/user-volume.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UserVolumeComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__ = __webpack_require__("../../../../../src/classes/wrappers/HighCharts.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var UserVolumeComponent = /** @class */ (function () {
    function UserVolumeComponent() {
    }
    UserVolumeComponent.prototype.ngOnInit = function () {
        this.data = [{
                name: 'Positivo',
                data: [null, null, null, null, null, 6, 11, 32, 110, 235, 369, 640,
                    1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
                    27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
                    26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
                    24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586,
                    22380, 21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950,
                    10871, 10824, 10577, 10527, 10475, 10421, 10358, 10295, 10104]
            }, {
                name: 'Negativo',
                data: [null, null, null, null, null, null, null, null, null, null,
                    5, 25, 50, 120, 150, 200, 426, 660, 869, 1060, 1605, 2471, 3322,
                    4238, 5221, 6129, 7089, 8339, 9399, 10538, 11643, 13092, 14478,
                    15915, 17385, 19055, 21205, 23044, 25393, 27935, 30062, 32049,
                    33952, 35804, 37431, 39197, 45000, 43000, 41000, 39000, 37000,
                    35000, 33000, 31000, 29000, 27000, 25000, 24000, 23000, 22000,
                    21000, 20000, 19000, 18000, 18000, 17000, 16000]
            }];
        this.highcharts = new __WEBPACK_IMPORTED_MODULE_1__classes_wrappers_HighCharts__["a" /* HighCharts */]();
        this.highcharts.area("perception-by-time", this.data);
    };
    UserVolumeComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-user-volume',
            template: __webpack_require__("../../../../../src/app/main-panel/user-volume/user-volume.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-panel/user-volume/user-volume.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], UserVolumeComponent);
    return UserVolumeComponent;
}());



/***/ }),

/***/ "../../../../../src/app/main-topics-panel/main-topics-panel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".date-time {\n    /* float: right; */\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/main-topics-panel/main-topics-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"\">\n    <span class=\"badge badge-pill badge-primary b-3d\">Primary</span>\n    <span class=\"badge badge-pill badge-secondary b-3d\">Secondary</span>\n    <span class=\"badge badge-pill badge-success b-3d\">Success</span>\n    <span class=\"badge badge-pill badge-danger b-3d\">Danger</span>\n    <span class=\"badge badge-pill badge-warning b-3d\">Warning</span>\n    <span class=\"badge badge-pill badge-info b-3d\">Info</span>\n    <span class=\"badge badge-pill badge-light b-3d\">Light</span>\n    <span class=\"badge badge-pill badge-dark b-3d\">Dark</span>\n    <!-- <span class=\"small date-time text-left\">{{ date.toString() }}</span> -->\n</div>\n<hr>"

/***/ }),

/***/ "../../../../../src/app/main-topics-panel/main-topics-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MainTopicsPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MainTopicsPanelComponent = /** @class */ (function () {
    function MainTopicsPanelComponent() {
    }
    MainTopicsPanelComponent.prototype.ngOnInit = function () {
        this.date = new Date();
    };
    MainTopicsPanelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-main-topics-panel',
            template: __webpack_require__("../../../../../src/app/main-topics-panel/main-topics-panel.component.html"),
            styles: [__webpack_require__("../../../../../src/app/main-topics-panel/main-topics-panel.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MainTopicsPanelComponent);
    return MainTopicsPanelComponent;
}());



/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.html":
/***/ (function(module, exports) {

module.exports = "<nav class=\"navbar navbar-expand-lg navbar-dark bg-primary\">\n    <div class=\"container-fluid\">\n        <a class=\"navbar-brand\" href=\"#\">Notybook | AI</a>\n        <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarColor01\" aria-controls=\"navbarColor01\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n            <span class=\"navbar-toggler-icon\"></span>\n        </button>\n\n        <div class=\"collapse navbar-collapse\" id=\"navbarColor01\">\n            <ul class=\"navbar-nav mr-auto\">\n                <li class=\"nav-item active\">\n                    <a class=\"nav-link\" href=\"#\">Inicio <span class=\"sr-only\">(current)</span></a>\n                </li>\n                <!--<li class=\"nav-item\">\n                    <a class=\"nav-link\" href=\"#\">Tendencias</a>\n                </li>\n                <li class=\"nav-item\">\n                    <a class=\"nav-link\" href=\"#\">Contactanos</a>\n                </li>-->\n            </ul>\n            <!-- <form class=\"form-inline my-2 my-lg-0\">\n          <input class=\"form-control mr-sm-2\" type=\"text\" placeholder=\"Search\">\n          <button class=\"btn btn-secondary my-2 my-sm-0\" type=\"submit\">Search</button>\n      </form> -->\n        </div>\n    </div>\n</nav>"

/***/ }),

/***/ "../../../../../src/app/navbar/navbar.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NavbarComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavbarComponent = /** @class */ (function () {
    function NavbarComponent() {
    }
    NavbarComponent.prototype.ngOnInit = function () {
    };
    NavbarComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-navbar',
            template: __webpack_require__("../../../../../src/app/navbar/navbar.component.html"),
            styles: [__webpack_require__("../../../../../src/app/navbar/navbar.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], NavbarComponent);
    return NavbarComponent;
}());



/***/ }),

/***/ "../../../../../src/app/quick-access/quick-access.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".btn-hover-custom-1:hover {\n    color: 'white' !important;\n}", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/quick-access/quick-access.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"row g-mb-25\">\n    <div class=\"col-md-1\"></div>\n    <div *ngFor=\"let keyword of _keywords\" class=\"col-sm-2\">\n        <a class=\"btn btn-primary\" role=\"button\" (click)=\"get_user_info(keyword)\">@{{keyword}}</a>\n    </div>\n</div>\n\n<!-- <mat-card class=\"example-card\"> -->\n<div class=\"row\">\n    <div class=\"col-md-6\">\n\n        <div class=\"u-timeline-v1 g-pa-15 g-pa-20--md bg-white\">\n            <!-- Timeline.Triangle -->\n            <div class=\"u-triangle-inclusive-v1--left g-hidden-md-down g-top-30 g-z-index-2\">\n                <div class=\"u-triangle-inclusive-v1--left__front g-brd-white-left\"></div>\n                <div class=\"u-triangle-inclusive-v1--left__back g-brd-gray-light-v4-left\"></div>\n            </div>\n            <!-- End Timeline.Triangle -->\n\n            <!-- Timeline. Item. Header -->\n            <header class=\"media g-mb-30\">\n                <div class=\"d-flex align-self-center\">\n                    <div class=\"media\">\n                        <a class=\"d-flex align-self-center g-mr-20\" href=\"#!\">\n                            <img class=\"rounded-circle g-width-45 g-width-55--lg g-height-45 g-height-55--lg\" [src]=\"data.status.user.profile_image_url_https\" alt=\"Image Description\">\n                        </a>\n\n                        <div class=\"media-body align-self-center\">\n                            <h3 class=\"g-font-weight-300 g-font-size-16 g-color-black g-mb-5\">{{data.status.user.name}}</h3>\n                            <em class=\"d-block g-font-style-normal g-font-weight-300 g-color-gray-dark-v6\">Publicado en <a class=\"u-link-v5 g-color-secondary g-color-primary--hover\" [href]=\"data.status.id_str\">Twitter</a></em>\n                        </div>\n                    </div>\n                </div>\n\n                <div class=\"media-body d-flex justify-content-end\">\n                    <div class=\"g-pos-rel g-z-index-2\">\n                        <a id=\"dropDown1Invoker\" class=\"u-link-v5 g-line-height-0 g-font-size-24 g-color-gray-light-v6 g-color-lightblue-v3--hover g-color-lightblue-v3--focus\" href=\"#!\" aria-controls=\"dropDown1\" aria-haspopup=\"true\" aria-expanded=\"false\" data-dropdown-event=\"click\"\n                            data-dropdown-target=\"#dropDown1\" data-dropdown-type=\"jquery-slide\">\n                            <i class=\"hs-admin-more-alt g-ml-20\"></i>\n                        </a>\n                    </div>\n                </div>\n            </header>\n            <!-- End Timeline. Item. Header -->\n\n            <!-- <img class=\"img-fluid w-100 g-mb-20\" [src]=\"data.status.user.profile_banner_url\" alt=\"Image Description\"> -->\n\n            <h3 class=\"g-font-weight-300 g-font-size-18 g-color-black g-mb-8\">#Trino mas comentado</h3>\n            <p class=\"g-color-gray-dark-v6 g-mb-10\">\n                {{ data.status.full_text }}\n            </p>\n\n            <!-- <a *ngIf=\"data.status.entities.media\" class=\"d-flex align-items-center u-link-v5 g-color-lightblue-v3 g-color-lightblue-v4--hover\" [href]=\"data.status.entities.media[0].display_url\">\n                <i class=\"hs-admin-link g-font-size-16 g-mr-5\"></i> {{data.status.entities.media[0].display_url}}\n            </a> -->\n\n            <hr class=\"d-flex g-brd-gray-light-v7 g-my-20\">\n\n            <!-- Timeline. Item. Footer -->\n            <footer class=\"d-flex\">\n                <a class=\"d-flex align-items-center u-link-v5 g-parent g-color-gray-dark-v6 g-mr-10 g-mr-20--sm\" href=\"#!\">\n                    <i class=\"hs-admin-heart g-font-size-16 g-color-gray-light-v6 g-color-lightblue-v4--parent-hover\"></i>\n                    <span class=\"g-hidden-lg-down g-font-weight-300 g-color-lightblue-v4--parent-hover g-ml-5\">Me Gusta</span>\n                </a>\n                <a class=\"d-flex align-items-center u-link-v5 g-parent g-color-gray-dark-v6 g-mr-10 g-mr-20--sm\" href=\"#!\">\n                    <i class=\"hs-admin-share g-font-size-16 g-color-gray-light-v6 g-color-lightblue-v4--parent-hover\"></i>\n                    <span class=\"g-hidden-lg-down g-font-weight-300 g-color-lightblue-v4--parent-hover g-ml-5\">Compartir</span>\n                </a>\n                <a class=\"d-flex align-items-center u-link-v5 g-parent g-color-gray-dark-v6 g-mr-10 g-mr-20--sm\" href=\"#!\">\n                    <i class=\"hs-admin-comments g-font-size-16 g-color-gray-light-v6 g-color-lightblue-v4--parent-hover\"></i>\n                    <span class=\"g-hidden-lg-down g-font-weight-300 g-color-lightblue-v4--parent-hover g-ml-5\">Comentar</span>\n                </a>\n                <!-- <a class=\"d-flex align-items-center u-link-v5 g-parent g-color-gray-dark-v6 ml-auto\" href=\"#!\">\n                    <i class=\"hs-admin-eye g-font-size-16 g-color-gray-light-v6 g-color-lightblue-v4--parent-hover\"></i>\n                    <span class=\"g-hidden-lg-down g-font-weight-300 g-color-lightblue-v4--parent-hover g-ml-5\">Hide</span>\n                </a> -->\n            </footer>\n            <!-- End Timeline. Item. Footer -->\n        </div>\n    </div>\n\n    <div class=\"col-md-6\">\n        <div [style.height]=\"'400px'\" [style.overflow]=\"'auto'\" class=\"g-brd-around g-brd-gray-light-v7 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30\">\n            <div *ngFor=\"let post of data.comments.slice(1, 30)\" class=\"noty_bar noty_type__error noty_theme__unify--v1--light g-mb-25\">\n                <div class=\"noty_body\" *ngIf=\"post.sentiment.positive >= 0.55\">\n                    <div class=\"g-mr-20\">\n                        <div class=\"noty_body__icon\">\n                            <i [style.color]='\"#8eb7f9\"' class=\"fa fa-thumbs-up\"></i>\n                        </div>\n                    </div>\n                    <div>\n                        <strong> ({{post.sentiment.positive.toFixed(2)}}) </strong> {{ post._text }}\n                    </div>\n                </div>\n                <div class=\"noty_body\" *ngIf=\"post.sentiment.positive <= 0.45\">\n                    <div class=\"g-mr-20\">\n                        <div class=\"noty_body__icon\">\n                            <i class=\"fa fa-thumbs-down\"></i>\n                        </div>\n                    </div>\n                    <div>\n                        <strong> ({{post.sentiment.positive.toFixed(2)}}) </strong> {{ post._text }}\n                    </div>\n                </div>\n\n                <div class=\"noty_body\" *ngIf=\"post.sentiment.positive > 0.45 && post.sentiment.positive < 0.55\">\n                    <div class=\"g-mr-20\">\n                        <div class=\"noty_body__icon\">\n                            <i [style.color]=\"black\" class=\"fa fa-arrows-h\"></i>\n                        </div>\n                    </div>\n                    <div>\n                        <strong> ({{post.sentiment.positive.toFixed(2)}}) </strong> {{ post._text }}\n                    </div>\n                </div>\n\n            </div>\n        </div>\n    </div>\n</div>\n\n<!-- </mat-card> -->"

/***/ }),

/***/ "../../../../../src/app/quick-access/quick-access.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return QuickAccessComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_tweet_service__ = __webpack_require__("../../../../../src/services/tweet.service.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__classes_wrappers_LdaModel__ = __webpack_require__("../../../../../src/classes/wrappers/LdaModel.ts");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var QuickAccessComponent = /** @class */ (function () {
    function QuickAccessComponent(tweetService) {
        this.tweetService = tweetService;
    }
    QuickAccessComponent.prototype.ngOnInit = function () {
        this.lda = new __WEBPACK_IMPORTED_MODULE_2__classes_wrappers_LdaModel__["a" /* LDA */]();
        this.data = { comments: [] };
    };
    QuickAccessComponent.prototype.ngOnChanges = function (changes) {
        // historical data
        if (changes['_keywords']) {
            if (this._keywords) {
                this.get_user_info(this._keywords[0]);
                // this.retrieve_historical_data(this._keywords);
            }
        }
    };
    QuickAccessComponent.prototype.get_user_info = function (keyword) {
        var _this = this;
        this.tweetService.most_voted_tweet(keyword).subscribe(function (res) {
            // console.log(res);
            // res['topics'] = this.lda.topics(res.comments.map(e => e._text));
            _this.data = res;
            console.log(_this.data);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
        __metadata("design:type", Object)
    ], QuickAccessComponent.prototype, "_keywords", void 0);
    QuickAccessComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-quick-access',
            template: __webpack_require__("../../../../../src/app/quick-access/quick-access.component.html"),
            styles: [__webpack_require__("../../../../../src/app/quick-access/quick-access.component.css")]
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_tweet_service__["a" /* TweetService */]])
    ], QuickAccessComponent);
    return QuickAccessComponent;
}());



/***/ }),

/***/ "../../../../../src/app/right-panel/mysentiment/mysentiment.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/right-panel/mysentiment/mysentiment.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\">\n    <div class=\"bg-white d-flex justify-content-start g-brd-around g-brd-gray-light-v1 g-pa-20 g-mb-12\">\n        <!-- <img class=\"card-img-top\" src=\"...\" alt=\"Card image cap\"> -->\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">Analysis de polaridad</h5>\n            <textarea rows=\"3\">Inserta cualquier texto aqui y obtendras el indice de polaridad</textarea>\n            <a href=\"#\" class=\"btn btn-primary\">Obtener Polaridad</a>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/right-panel/mysentiment/mysentiment.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MysentimentComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MysentimentComponent = /** @class */ (function () {
    function MysentimentComponent() {
    }
    MysentimentComponent.prototype.ngOnInit = function () {
    };
    MysentimentComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-mysentiment',
            template: __webpack_require__("../../../../../src/app/right-panel/mysentiment/mysentiment.component.html"),
            styles: [__webpack_require__("../../../../../src/app/right-panel/mysentiment/mysentiment.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MysentimentComponent);
    return MysentimentComponent;
}());



/***/ }),

/***/ "../../../../../src/app/right-panel/mytags/mytags.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/right-panel/mytags/mytags.component.html":
/***/ (function(module, exports) {

module.exports = "<div class=\"col-md-12\">\n    <div class=\"bg-white d-flex justify-content-start g-brd-around g-brd-gray-light-v1 g-pa-20 g-mb-12\">\n        <!-- <img class=\"card-img-top\" src=\"...\" alt=\"Card image cap\"> -->\n        <div class=\"card-body\">\n            <h5 class=\"card-title\">Analysis de polaridad</h5>\n            <p class=\"card-text\">Inserta cualquier texto aqui y obtendras el indice de polaridad.</p>\n            <a href=\"#\" class=\"btn btn-primary\">Obtener polaridad</a>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "../../../../../src/app/right-panel/mytags/mytags.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MytagsComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var MytagsComponent = /** @class */ (function () {
    function MytagsComponent() {
    }
    MytagsComponent.prototype.ngOnInit = function () {
    };
    MytagsComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-mytags',
            template: __webpack_require__("../../../../../src/app/right-panel/mytags/mytags.component.html"),
            styles: [__webpack_require__("../../../../../src/app/right-panel/mytags/mytags.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], MytagsComponent);
    return MytagsComponent;
}());



/***/ }),

/***/ "../../../../../src/app/right-panel/right-panel.component.css":
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__("../../../../css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ "../../../../../src/app/right-panel/right-panel.component.html":
/***/ (function(module, exports) {

module.exports = "<!-- <app-mysentiment></app-mysentiment> -->\n<!-- <app-mytags></app-mytags> -->\n\n<a class=\"twitter-timeline\" data-lang=\"es\" data-width=\"100%\" data-height=\"600\" data-theme=\"light\" data-link-color=\"#981CEB\" href=\"https://twitter.com/notybookcity?ref_src=twsrc%5Etfw\"></a>\n<script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script>\n\n<div *ngIf=\"render\" class=\"card\">\n\n    <ng4-twitter-timeline></ng4-twitter-timeline>\n\n</div>\n\n<div class=\"col-md-12 no-gutters\" *ngIf=\"!render\">\n    <div class=\"timeline-wrapper col-md-12\">\n        <div class=\"timeline-item\">\n            <div class=\"animated-background\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"col-md-12\" hidden>\n    <a href=\"https://clustrmaps.com/site/1a3ur\" title=\"Visit tracker\"><img src=\"//www.clustrmaps.com/map_v2.png?d=bQoFjhvLW2Tph6w7Bqi-ynzJPE542-ymYizGP53-Jh8&cl=ffffff\"></a>\n</div>\n<br>\n<div> \n    <a href=\"https://twitter.com/notybookcity?ref_src=twsrc%5Etfw\" class=\"twitter-follow-button\" data-show-count=\"false\">Follow @notybookcity</a><script async src=\"https://platform.twitter.com/widgets.js\" charset=\"utf-8\"></script> \n</div>\n\n\n"

/***/ }),

/***/ "../../../../../src/app/right-panel/right-panel.component.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RightPanelComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var RightPanelComponent = /** @class */ (function () {
    function RightPanelComponent() {
    }
    RightPanelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.render = false;
        setTimeout(function () {
            _this.render = true;
        }, 1000);
    };
    RightPanelComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-right-panel',
            template: __webpack_require__("../../../../../src/app/right-panel/right-panel.component.html"),
            styles: [__webpack_require__("../../../../../src/app/right-panel/right-panel.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], RightPanelComponent);
    return RightPanelComponent;
}());



/***/ }),

/***/ "../../../../../src/classes/wrappers/EventDrops.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EventDrops; });
var d3 = __webpack_require__("../../../../d3/build/d3.node.js");
var eventDrops = __webpack_require__("../../../../event-drops/dist/index.js");
var chart = eventDrops({ d3: d3 });
var EventDrops = /** @class */ (function () {
    function EventDrops() {
        // Example display: September 4 1986 8:30 PM
        this.humanizeDate = function (date) {
            var monthNames = [
                'Ene.',
                'Feb.',
                'Mar',
                'Abr.',
                'May',
                'Jun',
                'Jul.',
                'Ago.',
                'Sep.',
                'Oct.',
                'Nov.',
                'Dic.',
            ];
            return "\n            " + monthNames[date.getMonth()] + " " + date.getDate() + " " + date.getFullYear() + "\n            " + date.getHours() + ":" + date.getMinutes() + "\n        ";
        };
    }
    EventDrops.prototype.parse_data = function () {
    };
    EventDrops.prototype.render = function (repositories, _placeholder) {
        var _this = this;
        // const repositories = require('./data.json');
        var numberCommitsContainer = document.getElementById(_placeholder + '_numberCommits');
        var zoomStart = document.getElementById(_placeholder + '_zoomStart');
        var zoomEnd = document.getElementById(_placeholder + '_zoomEnd');
        var repositoriesData = repositories.map(function (repository) { return ({
            name: repository.name,
            data: repository.commits,
        }); });
        var updateCommitsInformation = function (chart) {
            var filteredData = chart
                .filteredData()
                .reduce(function (total, repo) { return total.concat(repo.data); }, []);
            numberCommitsContainer.textContent = (5 * filteredData.length).toString();
            zoomStart.textContent = _this.humanizeDate(chart.scale().domain()[0]);
            zoomEnd.textContent = _this.humanizeDate(chart.scale().domain()[1]);
        };
        var tooltip = d3
            .select('body')
            .append('div')
            .classed('tooltip', true)
            .style('opacity', 0);
        var chart = eventDrops({
            d3: d3,
            metaballs: {
                blurDeviation: 5,
            },
            zoom: {
                onZoomEnd: function () { return updateCommitsInformation(chart); },
            },
            drop: {
                radius: 3,
                date: function (d) { return new Date(d.date); },
                onMouseOver: function (commit) {
                    tooltip
                        .transition()
                        .duration(200)
                        .style('opacity', 1);
                    tooltip
                        .html("\n                        <div class=\"col-md-8\">\n                            <div class=\"box\">\n                                <div class=\"box-body\" style=\"background:#FFF\">\n\n                                        <li class=\"d-flex justify-content-start g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-10\">\n                                            <div class=\"g-mt-2\">\n                                                <img class=\"g-width-50 g-height-50 rounded-circle mCS_img_loaded\" src=\"" + commit.author.image + "\" alt=\"Image Description\">\n                                            </div>\n                                            <div class=\"align-self-center g-px-10\">\n                                                <h5 class=\"h6 g-font-weight-600 g-color-black g-mb-3\">\n                                                    <span class=\"g-mr-5\">@" + commit.author.name + "</span>\n                                                    <small class=\"g-font-size-12 g-color-blue\">" + _this.humanizeDate(new Date(commit.date)) + "</small>\n                                                </h5>\n                                                <p class=\"m-0\">" + commit.message + "</p>\n                                            </div>\n                                        </li>\n\n                                </div>\n                            </div>\n                        </div>\n                        ")
                        .style('left', d3.event.pageX - 10 + "px")
                        .style('top', d3.event.pageY + 10 + "px");
                },
                onMouseOut: function () {
                    tooltip
                        .transition()
                        .duration(500)
                        .style('opacity', 0);
                },
            },
            label: {
                padding: 20,
                text: function (d) { return d.name + " (" + 10 * d.data.length + ")"; },
                width: 150,
            },
            margin: {
                top: 40,
                right: 20,
                bottom: 40,
                left: 10,
            },
            range: {
                start: new Date(new Date().getTime() - 4 * 60 * 60 * 1000),
                end: new Date().getTime() - 0 * 60 * 60 * 1000
            }
        });
        d3
            .select('#_historical_sentiment_chart')
            .data([repositoriesData])
            .call(chart);
        // console.log('EvenDrop');
        // console.log(repositoriesData);
    };
    return EventDrops;
}());



/***/ }),

/***/ "../../../../../src/classes/wrappers/HighCharts.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HighCharts; });
var highcharts = __webpack_require__("../../../../highcharts/highcharts.js");
__webpack_require__("../../../../highcharts/modules/wordcloud.js")(highcharts);
var HighCharts = /** @class */ (function () {
    function HighCharts() {
    }
    HighCharts.prototype.compare = function (a, b) {
        if (a.y < b.y) {
            return 1;
        }
        if (a.y > b.y) {
            return -1;
        }
        return 0;
    };
    HighCharts.prototype.barplot = function (selector, data) {
        // console.log(data)
        data[0].data.sort(this.compare);
        highcharts.chart(selector, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'bar',
                width: null,
                height: data.length * 15 + 300,
                marginTop: 10,
                borderWidth: 0,
                borderColor: '#000'
            },
            title: {
                text: null,
                margin: 0
            },
            xAxis: {
                visible: true,
                offset: 2,
                color: '#000',
                linewidth: 1,
                labels: {
                    enabled: true,
                    style: {
                        color: '#000'
                    }
                },
                categories: data[0].data.map(function (e) { return e.name; })
            },
            yAxis: {
                visible: false,
                title: {
                    text: null
                    // align: 'high'
                },
                labels: {
                    enabled: true
                },
                min: 0,
                startOnTick: true,
                minorGridLineWidth: 1,
                majorGridLineWidth: 1,
                gridLineWidth: 1,
                // gridLineColor: '#000',
                tickWidth: 0
            },
            pane: {},
            tooltip: {
                enabled: false,
                pointFormat: '{point.y:.1f}%'
            },
            plotOptions: {
                series: {
                    colorByPoint: true,
                    groupPadding: 0,
                    stacking: 'normal',
                    borderColor: '#000',
                    borderWidth: 0
                },
                bar: {
                    borderRadius: 0,
                    showInLegend: true,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: 0,
                        format: '({point.y:.1f} %)'
                    }
                }
            },
            series: data,
            credits: {
                enabled: false
            },
            legend: {
                enabled: false
            }
        });
    };
    HighCharts.prototype.column = function (selector, data) {
        highcharts.chart(selector, {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'column',
                width: null,
                height: data.length * 15 + 300,
                marginTop: 10,
                borderWidth: null
            },
            title: {
                text: null,
                margin: 0
            },
            xAxis: {
                visible: false,
                offset: 2,
                color: null,
                linewidth: 0,
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                visible: false,
                title: {
                    text: 'Comentarios'
                },
                labels: {
                    enabled: false
                },
                min: -1,
                startOnTick: false,
                minorGridLineWidth: 0,
                majorGridLineWidth: 0,
                gridLineWidth: 0,
                tickWidth: 1
            },
            pane: {
                center: ['50%', '50%']
            },
            tooltip: {
                enabled: false,
                pointFormat: '{series.name}'
            },
            plotOptions: {
                series: {},
                column: {
                    borderRadius: 5,
                    showInLegend: false,
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        distance: 10,
                        format: '<b>{point.name}</b> ({point.y:.1f} %)'
                    }
                }
            },
            series: data,
            credits: {
                enabled: false
            }
        });
    };
    HighCharts.prototype.wordcloud = function (selector, data) {
        highcharts.chart(selector, {
            chart: {
                plotBorderWidth: 0
            },
            series: [
                {
                    type: 'wordcloud',
                    data: data,
                    name: 'Usuarios'
                }
            ],
            title: {
                text: null
            },
            credits: {
                enabled: false
            }
        });
    };
    HighCharts.prototype.area = function (selector, data) {
        highcharts.chart(selector, {
            chart: {
                type: 'line',
                width: null,
                height: data.length * 15 + 300,
                marginTop: 10
                // marginLeft: 2,
                // marginRight: 2,
                // marginBottom: 40
            },
            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                type: 'datetime',
                allowDecimals: false,
                labels: {
                    formatter: function () {
                        return this.value; // clean, unformatted number for year
                    }
                }
            },
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return this.value / 1000 + 'k';
                    }
                }
            },
            tooltip: {},
            plotOptions: {
                area: {
                    pointStart: 1940,
                    marker: {
                        enabled: false,
                        symbol: 'circle',
                        radius: 2,
                        states: {
                            hover: {
                                enabled: true
                            }
                        }
                    }
                }
            },
            series: data,
            credits: {
                enabled: false
            }
        });
    };
    HighCharts.prototype.area_spline = function (selector, data) {
        highcharts.chart(selector, {
            chart: {
                type: 'areaspline',
                height: data.length * 15 + 191
            },
            title: {
                text: null
            },
            legend: {
                layout: 'horizontal',
                align: 'center',
                verticalAlign: 'top',
                // x: 150,
                // y: 100,
                floating: true,
                borderWidth: 0,
                backgroundColor: 'rgba(255,255,255,0)'
            },
            xAxis: {
                categories: data[0].categories
                // plotBands: [
                //   {
                //     // visualize the weekend
                //     from: 4.5,
                //     to: 5.5,
                //     color: 'rgba(68, 170, 213, .2)'
                //   }
                // ]
            },
            yAxis: {
                title: {
                    text: null
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: ''
                // pointFormat: '{point.y:.2f}'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.1
                }
            },
            series: data
        });
    };
    return HighCharts;
}());



/***/ }),

/***/ "../../../../../src/classes/wrappers/LdaModel.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LDA; });
var lda = __webpack_require__("../../../../lda/lib/lda.js");
var LDA = /** @class */ (function () {
    function LDA() {
    }
    LDA.prototype.topics = function (documents) {
        // const text =
        //   'Cats are small. Dogs are big. Cats like to chase mice. Dogs like to eat bones.';
        // const documents = text.match(/[^\.!\?]+[\.!\?]+/g);
        return lda(documents, 3, 3, ['es', 'en']);
    };
    return LDA;
}());



/***/ }),

/***/ "../../../../../src/environments/environment.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
var environment = {
    production: true,
    base_api_url: 'http://18.221.42.69:57052/',
    polarity_api_url: 'http://18.221.42.69:37051/',
    tags_api_url: 'http://18.221.42.69:37050/',
};


/***/ }),

/***/ "../../../../../src/main.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__("../../../platform-browser-dynamic/esm5/platform-browser-dynamic.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__("../../../../../src/app/app.module.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs__ = __webpack_require__("../../../../hammerjs/hammer.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_hammerjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_hammerjs__);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["enableProdMode"])();
}
Object(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ "../../../../../src/services/contact.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_delay__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/delay.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ContactService = /** @class */ (function () {
    function ContactService(http) {
        this.http = http;
        this.base_api_url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].base_api_url;
    }
    ContactService.prototype.send_email = function (name, email, message) {
        return this.http.post(this.base_api_url + 'contact/sendEmail/', { name: name, email: email, message: message })
            .map(function (res) {
            console.log(res);
            return res.json();
        });
    };
    ContactService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], ContactService);
    return ContactService;
}());



/***/ }),

/***/ "../../../../../src/services/tweet.service.ts":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TweetService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__("../../../core/esm5/core.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__("../../../http/esm5/http.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__environments_environment__ = __webpack_require__("../../../../../src/environments/environment.ts");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_observable_of__ = __webpack_require__("../../../../rxjs/_esm5/add/observable/of.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_map__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/map.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_do__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/do.js");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_delay__ = __webpack_require__("../../../../rxjs/_esm5/add/operator/delay.js");
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var TweetService = /** @class */ (function () {
    function TweetService(http) {
        this.http = http;
        this.base_api_url = __WEBPACK_IMPORTED_MODULE_2__environments_environment__["a" /* environment */].base_api_url;
    }
    TweetService.prototype.sentiment_analysis = function (users, analysis_type) {
        return this.http
            .post(this.base_api_url + '/sentiment/retrieve/most_relevant/', {
            users: users,
            analysis_type: analysis_type
        })
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.topic_analysis = function (users, analysis_type) {
        return this.http
            .post(this.base_api_url + '/topics/retrieve/', {
            users: users,
            analysis_type: analysis_type
        })
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.current_keywords = function () {
        var _this = this;
        return this.http.get(this.base_api_url + 'retrieve/keywords').map(function (res) {
            _this._current_keywords = res.json();
        });
    };
    TweetService.prototype.tweet_history = function (keyword) {
        return this.http
            .get(this.base_api_url + 'retrieve/historical_data/' + keyword)
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.tweet_history_10h = function (keyword) {
        return this.http
            .get(this.base_api_url + 'retrieve/sentiment_10h/' + keyword)
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.tweet_history_total = function (keyword) {
        return this.http
            .get(this.base_api_url + 'retrieve/historical_total/' + keyword)
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.tweet_status = function (keyword) {
        return this.http
            .get(this.base_api_url + 'retrieve/profile_status/' + keyword)
            .map(function (res) {
            return res.json();
        });
    };
    TweetService.prototype.most_voted_tweet = function (keyword) {
        return this.http
            .get(this.base_api_url + 'retrieve/most_voted_tweet/' + keyword)
            .map(function (res) {
            return res.json();
        });
    };
    TweetService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Http */]])
    ], TweetService);
    return TweetService;
}());



/***/ }),

/***/ "../../../../lda/lib recursive ^\\.\\/stopwords_.*\\.js$":
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./stopwords_de.js": "../../../../lda/lib/stopwords_de.js",
	"./stopwords_en.js": "../../../../lda/lib/stopwords_en.js",
	"./stopwords_es.js": "../../../../lda/lib/stopwords_es.js"
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "../../../../lda/lib recursive ^\\.\\/stopwords_.*\\.js$";

/***/ }),

/***/ 0:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__("../../../../../src/main.ts");


/***/ })

},[0]);
//# sourceMappingURL=main.bundle.js.map